#pragma once

#include "../Core/DCoordinates3.h"

namespace cagd
{
    namespace d1_scalar
    {
        extern GLdouble scalar;
    }

    namespace d2_scalar
    {
        extern GLdouble scalar;
    }

    namespace spiral_on_cone
    {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace epicyloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble R, r;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace hypocyloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble R, r;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace helix
    {
        extern GLdouble u_min, u_max;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace curve_on_torus
    {
        extern GLdouble u_min, u_max;
        extern GLdouble R, r;
        extern GLdouble phi_ratio;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace curve_on_ellipsoid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble a, b, c;
        extern GLdouble phi_ratio;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace curve_on_hyperboloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble a, b, c;
        extern GLdouble v_ratio;
        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace archimedean_spiral
    {
        extern GLdouble u_min, u_max;
        extern GLdouble a, b;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace cone
    {
        extern GLdouble r_min, r_max;
        extern GLdouble theta_min, theta_max;
        DCoordinate3 d00(GLdouble,GLdouble);
        DCoordinate3 d10(GLdouble,GLdouble);
        DCoordinate3 d11(GLdouble,GLdouble);
    }

    namespace ellipsoid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble v_min, v_max;
        extern GLdouble a, b, c;
        DCoordinate3 d00(GLdouble,GLdouble);
        DCoordinate3 d10(GLdouble,GLdouble);
        DCoordinate3 d11(GLdouble,GLdouble);
    }

    namespace hyperboloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble v_min, v_max;
        extern GLdouble a, b, c;
        DCoordinate3 d00(GLdouble,GLdouble);
        DCoordinate3 d10(GLdouble,GLdouble);
        DCoordinate3 d11(GLdouble,GLdouble);
    }

    namespace elliptic_paraboloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble v_min, v_max;
        extern GLdouble a, b;
        DCoordinate3 d00(GLdouble,GLdouble);
        DCoordinate3 d10(GLdouble,GLdouble);
        DCoordinate3 d11(GLdouble,GLdouble);
    }

    namespace hyperboloic_paraboloid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble v_min, v_max;
        extern GLdouble a, b;
        DCoordinate3 d00(GLdouble,GLdouble);
        DCoordinate3 d10(GLdouble,GLdouble);
        DCoordinate3 d11(GLdouble,GLdouble);
    }

    namespace helicoid
    {
        extern GLdouble u_min, u_max;
        extern GLdouble v_min, v_max;
        extern GLdouble a;
        DCoordinate3 d00(GLdouble,GLdouble);
        DCoordinate3 d10(GLdouble,GLdouble);
        DCoordinate3 d11(GLdouble,GLdouble);
    }
}
