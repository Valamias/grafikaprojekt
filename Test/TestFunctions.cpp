#include <cmath>
#include "TestFunctions.h"
#include "../Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI;
GLdouble spiral_on_cone::u_max = +TWO_PI;

GLdouble d1_scalar::scalar = 1;
GLdouble d2_scalar::scalar = 1;


DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0) * d1_scalar::scalar;
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0) * d2_scalar::scalar;
}

GLdouble epicyloid::r = 5;
GLdouble epicyloid::R = 8;
GLdouble epicyloid::u_min = 0;
GLdouble epicyloid::u_max = (R+r) * +TWO_PI;

DCoordinate3 epicyloid::d0(GLdouble u)
{
    return DCoordinate3((r + R) * cos(u) - r * cos((R / r + 1) * u),(r + R) * sin(u) - r * sin((R / r + 1) * u), 0);
}

DCoordinate3 epicyloid::d1(GLdouble u)
{
    return DCoordinate3((r + R) * (sin((R / r + 1) * u) - sin(u)), -(r + R) * (cos((R / r + 1) * u) - cos(u)), 0) * d1_scalar::scalar;
}

DCoordinate3 epicyloid::d2(GLdouble u)
{
    return DCoordinate3((r + R) * ((R / r + 1) * cos((R / r + 1) * u) - cos(u)), -(r + R) * (sin(u) - (R / r + 1) * sin((R / r + 1) * u)), 0) * d2_scalar::scalar;
}

GLdouble hypocyloid::r = 1;
GLdouble hypocyloid::R = 4;
GLdouble hypocyloid::u_min = 0;
GLdouble hypocyloid::u_max = (R+r) * +TWO_PI;

DCoordinate3 hypocyloid::d0(GLdouble u)
{
    return DCoordinate3((R - r) * cos(u) + r * cos(((R - r) / r) * u),(R - r) * sin(u) - r * sin(((R - r) / r) * u), 0);
}

DCoordinate3 hypocyloid::d1(GLdouble u)
{
    return DCoordinate3((r - R) * (sin((R - r) * u / r) + sin(u)), (r - R) * (cos((R - r) * u / r) - cos(u)), 0) * d1_scalar::scalar;
}

DCoordinate3 hypocyloid::d2(GLdouble u)
{
    return DCoordinate3((r - R) * ((R - r) * cos((R - r) * u / r) / r + cos(u)), (r - R) * (sin(u) - (R - r) * sin((R - r) * u / r) / r), 0) * d2_scalar::scalar;
}


GLdouble helix::u_min = -TWO_PI;
GLdouble helix::u_max = +TWO_PI;

DCoordinate3 helix::d0(GLdouble u)
{
    return DCoordinate3(cos(u), sin(u), u);
}

DCoordinate3 helix::d1(GLdouble u)
{
    return DCoordinate3(-sin(u), cos(u), 1) * d1_scalar::scalar;
}

DCoordinate3 helix::d2(GLdouble u)
{
    return DCoordinate3(-cos(u), -sin(u), 0) * d2_scalar::scalar;
}

GLdouble curve_on_torus::u_min = 0;
GLdouble curve_on_torus::u_max = +TWO_PI;
GLdouble curve_on_torus::r = 1;
GLdouble curve_on_torus::R = 4;
GLdouble curve_on_torus::phi_ratio = 3;

DCoordinate3 curve_on_torus::d0(GLdouble u)
{
    return DCoordinate3((R + r * cos(u)) * cos(phi_ratio * u), (R + r * cos(u)) * sin(phi_ratio * u), r * sin(u));
}

DCoordinate3 curve_on_torus::d1(GLdouble u)
{
    return DCoordinate3(-phi_ratio * (r * cos(u) + R) * sin(phi_ratio * u) - r * sin(u) * cos(phi_ratio * u),
                        phi_ratio * (r * cos(u) + R) * cos(phi_ratio * u) - r * sin(u) * sin(phi_ratio * u),
                        r * sin(u)) * d1_scalar::scalar;
}

DCoordinate3 curve_on_torus::d2(GLdouble u)
{
    return DCoordinate3(2 * phi_ratio * r * sin(u) * sin(phi_ratio * u) + ((-phi_ratio * phi_ratio - 1) * r * cos(u) - R * phi_ratio * phi_ratio) * cos(phi_ratio * u),
                        -((phi_ratio * phi_ratio + 1) * r * cos(u) + R * phi_ratio * phi_ratio) * sin(phi_ratio * u) - 2 * phi_ratio * r * sin(u) * cos(phi_ratio * u),
                        -r * sin(u)) * d2_scalar::scalar;
}

GLdouble curve_on_ellipsoid::u_min = 0;
GLdouble curve_on_ellipsoid::u_max = TWO_PI;
GLdouble curve_on_ellipsoid::a = 4;
GLdouble curve_on_ellipsoid::b = 5;
GLdouble curve_on_ellipsoid::c = 6;
GLdouble curve_on_ellipsoid::phi_ratio = 1;

DCoordinate3 curve_on_ellipsoid::d0(GLdouble u)
{
    return DCoordinate3(a * sin(u) * cos(u * phi_ratio), b * sin(u) * sin(u * phi_ratio), c * cos(u));
}

DCoordinate3 curve_on_ellipsoid::d1(GLdouble u)
{
    return DCoordinate3(-a * (phi_ratio  * sin(u) * sin(phi_ratio * u) - cos(u) * cos(phi_ratio  * u)),
                        b * (cos(u) * sin(phi_ratio  * u) + phi_ratio  * sin(u) * cos(phi_ratio * u)),
                        -c * sin(u)) * d1_scalar::scalar;
}

DCoordinate3 curve_on_ellipsoid::d2(GLdouble u)
{
    return DCoordinate3(-a * ((phi_ratio * phi_ratio + 1) * sin(u) * cos(phi_ratio * u) + 2 * phi_ratio * cos(u) * sin(phi_ratio * u)),
                        -b * ((phi_ratio * phi_ratio + 1) * sin(u) * sin(phi_ratio * u) - 2 * phi_ratio * cos(u) * cos(phi_ratio * u)),
                        -c * cos(u)) * d2_scalar::scalar;
}

GLdouble curve_on_hyperboloid::u_min = 0;
GLdouble curve_on_hyperboloid::u_max = TWO_PI;
GLdouble curve_on_hyperboloid::a = 1;
GLdouble curve_on_hyperboloid::b = 1.7;
GLdouble curve_on_hyperboloid::c = 1.6;
GLdouble curve_on_hyperboloid::v_ratio = 0.3;

DCoordinate3 curve_on_hyperboloid::d0(GLdouble u)
{
    return DCoordinate3(a * cosh(v_ratio * u) * cos(u), b * cosh(v_ratio * u) * sin(u), c * sinh(v_ratio * u));
}

DCoordinate3 curve_on_hyperboloid::d1(GLdouble u)
{
    return DCoordinate3(a * (v_ratio  * cos(u) * sinh(v_ratio * u) - sin(u) * cosh(v_ratio  * u)),
                        b * (cos(u) * cosh(v_ratio  * u) + v_ratio  * sin(u) * sinh(v_ratio * u)),
                        c * v_ratio * cosh(v_ratio * u)) * d1_scalar::scalar;
}

DCoordinate3 curve_on_hyperboloid::d2(GLdouble u)
{
    return DCoordinate3(-a * ((1 - v_ratio * v_ratio) * cos(u) * cosh(v_ratio * u) + 2 * v_ratio * sin(u) * sinh(v_ratio * u)),
                        b * ((v_ratio * v_ratio - 1) * sin(u) * cosh(v_ratio * u) - 2 * v_ratio * cos(u) * sinh(v_ratio * u)),
                        c * v_ratio * v_ratio * sinh(v_ratio * u)) * d2_scalar::scalar;
}

GLdouble archimedean_spiral::u_min = 0;
GLdouble archimedean_spiral::u_max = +3 * TWO_PI;
GLdouble archimedean_spiral::a = 2;
GLdouble archimedean_spiral::b = 1;

DCoordinate3 archimedean_spiral::d0(GLdouble u)
{
    return DCoordinate3((a + b * u) * cos(u), (a + b * u) * sin(u), 0);
}

DCoordinate3 archimedean_spiral::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(b * c - (b * u + a) * s, b * s + (b * u + a) * c, 0) * d1_scalar::scalar;
}

DCoordinate3 archimedean_spiral::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * b * s - (b * u + a) * c, 2.0 * b * c - (b * u + a) * s, 0) * d2_scalar::scalar;
}

GLdouble cone::r_min = -10;
GLdouble cone::r_max = 10;
GLdouble cone::theta_min = 0;
GLdouble cone::theta_max = TWO_PI;

DCoordinate3 cone::d00(GLdouble r, GLdouble theta)
{
    return DCoordinate3(cos(theta),sin(theta), r);
}

DCoordinate3 cone::d10(GLdouble r, GLdouble theta)
{
    return DCoordinate3(0,0, 1);
}

DCoordinate3 cone::d11(GLdouble r, GLdouble theta)
{
    return DCoordinate3( -sin(theta), cos(theta), 0);
}

GLdouble ellipsoid::u_min = 0;
GLdouble ellipsoid::u_max = PI;
GLdouble ellipsoid::v_min = 0;
GLdouble ellipsoid::v_max = TWO_PI;
GLdouble ellipsoid::a = 4;
GLdouble ellipsoid::b = 5;
GLdouble ellipsoid::c = 6;

DCoordinate3 ellipsoid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(a * sin(u) * cos(v), b * sin(u) * sin(v), c * cos(u));
}

DCoordinate3 ellipsoid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(a * cos(u) * cos(v), b * cos(u) * sin(v), -c * sin(u));
}

DCoordinate3 ellipsoid::d11(GLdouble u, GLdouble v)
{
    return DCoordinate3(-a * sin(u) * sin(v), b * sin(u) * cos(v), 0);
}

GLdouble hyperboloid::u_min = -10;
GLdouble hyperboloid::u_max = 10;
GLdouble hyperboloid::v_min = 0;
GLdouble hyperboloid::v_max = TWO_PI;
GLdouble hyperboloid::a = 1;
GLdouble hyperboloid::b = 1.7;
GLdouble hyperboloid::c = 1.6;

DCoordinate3 hyperboloid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(a * cosh(u) * cos(v), b * cosh(u) * sin(v), c * sinh(u));
}

DCoordinate3 hyperboloid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(a * sinh(u) * cos(v), b * sinh(u) * sin(v), c * cosh(u));
}

DCoordinate3 hyperboloid::d11(GLdouble u, GLdouble v)
{
    return DCoordinate3(-a * sinh(u) * sin(v), b * sinh(u) * cos(v), 0);
}

GLdouble elliptic_paraboloid::u_min = -5;
GLdouble elliptic_paraboloid::u_max =  5;
GLdouble elliptic_paraboloid::v_min = -5;
GLdouble elliptic_paraboloid::v_max =  5;
GLdouble elliptic_paraboloid::a = 1;
GLdouble elliptic_paraboloid::b = 1;

DCoordinate3 elliptic_paraboloid::d00(GLdouble u,GLdouble v)
{
    return DCoordinate3(u, v, u * u / (a * a) + v * v / (b * b));
}

DCoordinate3 elliptic_paraboloid::d10(GLdouble u,GLdouble v)
{
    return DCoordinate3(1, 0, 2 * u / (a * a));
}

DCoordinate3 elliptic_paraboloid::d11(GLdouble u,GLdouble v)
{
    return DCoordinate3(0, 1, 2 * v / (b * b));
}

GLdouble hyperboloic_paraboloid::u_min = -5;
GLdouble hyperboloic_paraboloid::u_max =  5;
GLdouble hyperboloic_paraboloid::v_min = -5;
GLdouble hyperboloic_paraboloid::v_max =  5;
GLdouble hyperboloic_paraboloid::a = 1;
GLdouble hyperboloic_paraboloid::b = 1;

DCoordinate3 hyperboloic_paraboloid::d00(GLdouble u,GLdouble v)
{
    return DCoordinate3(u, v, u * u / (a * a) - v * v / (b * b));
}

DCoordinate3 hyperboloic_paraboloid::d10(GLdouble u,GLdouble v)
{
    return DCoordinate3(1, 0, 2 * u / (a * a));
}

DCoordinate3 hyperboloic_paraboloid::d11(GLdouble u,GLdouble v)
{
    return DCoordinate3(0, 1, - 2 * v / (b * b));
}


GLdouble helicoid::u_min = -5;
GLdouble helicoid::u_max =  5;
GLdouble helicoid::v_min = -5;
GLdouble helicoid::v_max =  5;
GLdouble helicoid::a = 1;

DCoordinate3 helicoid::d00(GLdouble theta, GLdouble p)
{
    return DCoordinate3(p * cos(a * theta), p * sin(a * theta), theta);
}

DCoordinate3 helicoid::d10(GLdouble theta, GLdouble p)
{
    return DCoordinate3(- a * p * sin(a * theta), a * p * cos(a * theta), 1);
}

DCoordinate3 helicoid::d11(GLdouble theta, GLdouble p)
{
    return DCoordinate3(cos(a * theta), sin(a * theta), 0);
}
