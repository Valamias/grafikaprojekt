#pragma once

#include <GL/glew.h>
#include <QOpenGLWidget>
#include "../Core/GenericCurves3.h"
#include "../Parametric/ParametricCurves3.h"
#include "../Cyclic/CyclicCurves3.h"
#include "../Core/TriangulatedMeshes3.h"
#include <QTimer>
#include "../Core/ModelProperties.h"
#include "../Core/Lights.h"
#include "../Project/CubicBezierCurve.h"
#include "../Project/BezierPatch.h"
#include "../Core/ShaderPrograms.h"
#include <QOpenGLTexture>

namespace cagd
{
    class GLWidget: public QOpenGLWidget
    {
        Q_OBJECT

    private:

        // variables defining the projection matrix
        double       _aspect;            // aspect ratio of the rendering window
        double       _fovy;              // field of view in direction y
        double       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        double       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;

        // your other declarations

        int         _homework_id;

        GLuint                       _div_point_count = 100;
        bool                         _show_tangents = false;
        bool                         _show_acceleration_vectors = false;
        bool                         _show_control_points = false;
        bool                         _show_patch_control_points = false;
        bool                         _show_normal_vectors = false;
        bool                         _show_curves = true;
        bool                         _show_patches = true;
        bool                         _switch_curves_patches = false;
        bool                         _use_colors = false;
        bool                         _patch_tangent_visibility = false;
        bool                         _use_materials = true;
        bool                         _use_shaders = false;
        GLdouble                     _scale_tangents = 1.0;
        GLdouble                     _scale_acceleration_vectors = 1.0;

        Material*                    _materials[7] = { &MatFBBrass,
                                                       &MatFBGold,
                                                       &MatFBSilver,
                                                       &MatFBEmerald,
                                                       &MatFBPearl,
                                                       &MatFBRuby,
                                                       &MatFBTurquoise};
        GLuint                                  _selected_shader = 0;
        GLfloat                                 _reflScaleFactor = 1.0f;
        GLfloat                                 _reflSmoothing = 1.0f;
        GLfloat                                 _reflShading = 1.0f;
        GLint                                   _currShader = 0;
        RowMatrix<GLfloat>                      _toonOutlineColor;
        RowMatrix<ShaderProgram*>               _shaders;



        RowMatrix<GenericCurve3*>               _control_points;
        RowMatrix<double*>                      _poz_matrix;
        GLuint                                  _selected_Obj = 0;
        RowMatrix<GLdouble>                     _knot_factor;

        std::vector<CubicBezierCurve*>          _bezierc;
        GLuint                                  _bezier_count = 2;
        ColumnMatrix<GLdouble>                  _knot;
        GLuint                                  _bezier_div_point_count = 100;
        GLuint                                  _selected_bezier_curve = 0;
        GLuint                                  _selected_bezier_curve_point = 0;
        RowMatrix<ColumnMatrix<DCoordinate3>>   _bezier_points;
        TriangulatedMesh3                       _bezier_control_point_obj;
        GLdouble                                _control_scale = 0.01;
        GLdouble                                _normal_scale = 1.0;

        std::vector<BicubicBezierPatch*>        _bezierp;
        RowMatrix<GLdouble>                     _u_knot;
        ColumnMatrix<GLdouble>                  _v_knot;
        GLuint                                  _bezierp_count = 2;
        GLuint                                  _bezierp_div_point_count_u = 100;
        GLuint                                  _bezierp_div_point_count_v = 100;
        GLuint                                  _selected_bezier_patch = 0;
        GLuint                                  _selected_bezier_patch_pointX = 0;
        GLuint                                  _selected_bezier_patch_pointY = 0;
        RowMatrix<ColumnMatrix<RowMatrix<DCoordinate3>>>   _bezierp_points;
        GLdouble                                _control_scale_patch = 0.01;
        bool                                    _show_U_control_lines = false;
        bool                                    _show_V_control_lines = false;

        QOpenGLTexture*                         _ps_text = nullptr;
        boolean                                 _show_text = false;
        QPoint                                  _mouse_pos_start;
        ColumnMatrix<GLdouble> _mx;

        GLuint                                  _selected_light = 0;

        GLenum              _light_index[3] = {GL_LIGHT1, GL_LIGHT2, GL_LIGHT3};
        HCoordinate3        _light_position[3] = {HCoordinate3(0,0,1,0), HCoordinate3(0,0,1,0.5), HCoordinate3(0,0,1,0.5)};
        Color4              _light_ambient_intensity[3] = {Color4(0.4f,0.4f,0.4f), Color4(0.8f,0.8f,0.8f), Color4(0.8f,0.8f,0.8f)};
        Color4              _light_diffuse_intensity[3] = {Color4(0.8f,0.8f,0.8f), Color4(0.8f,0.8f,0.8f), Color4(0.8f,0.8f,0.8f)};
        Color4              _light_specular_intensity[3] = {Color4(1,1,1), Color4(1,1,1), Color4(1,1,1)};
        GLfloat             _light_constant_attenuation[3] = {1,1,1};
        GLfloat             _light_linear_attenuation[3]{0};
        GLfloat             _light_quadratic_attenuation[3]{0};
        HCoordinate3        _light_spot_direction[3] = {HCoordinate3(0,0,1), HCoordinate3(0,0,1), HCoordinate3(0,0,1)};
        GLfloat             _light_spot_cutoff[3] = {0, 0, 90};
        GLfloat             _light_spot_exponent[3]{0};
        DirectionalLight    _Dir_light = DirectionalLight(
                                            _light_index[0],
                                            _light_position[0],
                                            _light_ambient_intensity[0],
                                            _light_diffuse_intensity[0],
                                            _light_specular_intensity[0]
                                            );

        PointLight          _Point_light = PointLight(
                                            _light_index[1],
                                            _light_position[1],
                                            _light_ambient_intensity[1],
                                            _light_diffuse_intensity[1],
                                            _light_specular_intensity[1],
                                            _light_constant_attenuation[1],
                                            _light_linear_attenuation[1],
                                            _light_quadratic_attenuation[1]
                                            );

        Spotlight           _Spot_light = Spotlight(
                                            _light_index[2],
                                            _light_position[2],
                                            _light_ambient_intensity[2],
                                            _light_diffuse_intensity[2],
                                            _light_specular_intensity[2],
                                            _light_constant_attenuation[2],
                                            _light_linear_attenuation[2],
                                            _light_quadratic_attenuation[2],
                                            _light_spot_direction[2],
                                            _light_spot_cutoff[2],
                                            _light_spot_exponent[2]
                                            );

    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = 0);
        ~GLWidget();

        // redeclared virtual functions
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);
        bool _createAllBezierCurvesAndTheirImages();
        void _destroyAllExistingBezierCurvesAndTheirImages();
        bool _renderAllBezierCurves();
        bool _updateSelectedBezierCurve();
        bool _updateAllBezierCurves();
        bool _createAllBezierPatchesAndTheirImages();
        void _destroyAllExistingBezierPatchesAndTheirImages();
        bool _renderAllBezierPatches();
        bool _updateSelectedBezierPatch();
        bool _updateAllBezierPatches();


        GLdouble _get_bezier_X(int,int);
        GLdouble _get_bezier_Y(int,int);
        GLdouble _get_bezier_Z(int,int);

        GLdouble _get_bezierp_X(int,int,int);
        GLdouble _get_bezierp_Y(int,int,int);
        GLdouble _get_bezierp_Z(int,int,int);

        GLboolean extendCurve(int);
        GLboolean joinCurves(int,int,int);
        GLboolean mergeCurves(int,int,int);

        void addCurve();
        void deleteCurve();

        GLdouble        _get_knots(int);
        void            _set_knots();

        ModelProperties _get_selected_prop(int);
        GLuint          _get_bezier_line_count(int);

        GLboolean extendPatch(int);
        GLboolean mergePatches(int,int,int);
        GLboolean jointPatches(int,int,int);

        void addPatch();
        void deletePatch();

        void mouseMoveEvent(QMouseEvent* event);
        void mousePressEvent(QMouseEvent* event);
        void wheelEvent(QWheelEvent* event);

        void switch_curves_patches(bool);

        void translate_patch(GLdouble,GLdouble,GLdouble);
        void translate_whole(GLdouble,GLdouble,GLdouble);

        void set_light_pos(GLdouble,GLdouble,GLdouble,GLdouble);
        void set_light_ambience(GLdouble,GLdouble,GLdouble,GLdouble);
        void set_light_diffuse(GLdouble,GLdouble,GLdouble,GLdouble);
        void set_light_specular(GLdouble,GLdouble,GLdouble,GLdouble);
        void set_light_constant(GLdouble);
        void set_light_linear(GLdouble);
        void set_light_quadratic(GLdouble);
        void set_light_spot_dir(GLdouble,GLdouble,GLdouble);
        void set_light_spot_cutoff(GLdouble);
        void set_light_spot_exponent(GLdouble);

        int get_selected_bezier_patch_color_R();
        int get_selected_bezier_patch_color_G();
        int get_selected_bezier_patch_color_B();

        int get_selected_bezier_curve_color_R();
        int get_selected_bezier_curve_color_G();
        int get_selected_bezier_curve_color_B();


        int get_bezierc_count();
        int get_bezierp_count();

        void set_refl_scale(double value);
        void set_refl_smoothing(double value);
        void set_refl_shading(double value);

        void set_toon_color_R(double value);
        void set_toon_color_G(double value);
        void set_toon_color_B(double value);
        void set_toon_color_A(double value);

        void saveCurves();
        void loadCurves();

        int get_patch_shader();

        void savePatches();
        void loadPatches();

    public slots:
        // public event handling methods/slots
        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);

        void setVisibilityOfTangents(int);
        void setVisibilityOfAccelerationVectors(int);
        void setVisibilityOfControlPoints(int);
        void setVisibilityOfPatchControlPoints(int);
        void setVisibilityOfPatchUControlLines(int);
        void setVisibilityOfPatchVControlLines(int);
        void setVisibilityOfPatchNormalVectors(int);

        void setVisibilityOfPatchTangents(int);
        void setUseOfPatchColors(bool);
        void setUseOfPatchMaterials(bool);
        void setUseOfPatchShaders(bool);

        void set_selected_Obj(int value);
        void set_selected_Obj_Update(int);

        void setVisibilityOfCurves(int);

        void set_bezier_curve_point_X(double value);
        void set_bezier_curve_point_Y(double value);
        void set_bezier_curve_point_Z(double value);

        void set_selected_bezier_curve(int value);
        void set_selected_bezier_curve_point(int value);
        void set_control_point_scale(double value);
        void set_control_line_count(int value);

        void set_bezier_patch_point_X(double value);
        void set_bezier_patch_point_Y(double value);
        void set_bezier_patch_point_Z(double value);

        void set_selected_bezier_patch(int value);
        void set_selected_bezier_patch_pointX(int value);
        void set_selected_bezier_patch_pointY(int value);
        void set_control_point_patch_scale(double value);

        void setVisibilityOfTexture(int);

        void set_light_type(int);

        void set_selected_bezier_curve_color_R(int value);
        void set_selected_bezier_curve_color_G(int value);
        void set_selected_bezier_curve_color_B(int value);

        void set_selected_bezier_patch_color_R(int value);
        void set_selected_bezier_patch_color_G(int value);
        void set_selected_bezier_patch_color_B(int value);
        void set_selected_shader(int value);

        void set_selected_bezier_patch_material(int value);

    };
}
