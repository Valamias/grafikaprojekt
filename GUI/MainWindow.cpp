#include "MainWindow.h"
#include "../Core/ModelProperties.h"

#include <QHBoxLayout>
#include <string>

namespace cagd
{
    MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
    {
        setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */

        _side_widget = new SideWidget(this);

        _scroll_area = new QScrollArea(this);
        _scroll_area->setWidget(_side_widget);
        _scroll_area->setSizePolicy(_side_widget->sizePolicy());
        _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        _gl_widget = new GLWidget(this);

        centralWidget()->setLayout(new QHBoxLayout());
        centralWidget()->layout()->addWidget(_gl_widget);
        centralWidget()->layout()->addWidget(_scroll_area);

        connect(_side_widget->bezier_curve_ID, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_bezier_curve(int)));
        connect(_side_widget->bezier_curve_point_ID, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_bezier_curve_point(int)));

        connect(_side_widget->bezier_curve_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_curve_XYZ(int)));
        connect(_side_widget->bezier_curve_point_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_curve_point_XYZ(int)));

        connect(_side_widget->bezier_curve_point_X, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_bezier_curve_point_X(double)));
        connect(_side_widget->bezier_curve_point_Y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_bezier_curve_point_Y(double)));
        connect(_side_widget->bezier_curve_point_Z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_bezier_curve_point_Z(double)));

        connect(_side_widget->Tangent_Visibility,       SIGNAL(stateChanged(int)),          _gl_widget, SLOT(setVisibilityOfTangents(int)));
        connect(_side_widget->Acc_Vect_Visibility,      SIGNAL(stateChanged(int)),          _gl_widget, SLOT(setVisibilityOfAccelerationVectors(int)));
        connect(_side_widget->Control_Point_Visiblity,  SIGNAL(stateChanged(int)),          _gl_widget, SLOT(setVisibilityOfControlPoints(int)));
        connect(_side_widget->texture_visibility,       SIGNAL(stateChanged(int)),          _gl_widget, SLOT(setVisibilityOfTexture(int)));

        connect(_side_widget->bezier_control_point_scale, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_control_point_scale(double)));

        connect(_side_widget->bezier_patch_ID, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch(int)));
        connect(_side_widget->bezier_patch_point_ID_X, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch_pointX(int)));
        connect(_side_widget->bezier_patch_point_ID_Y, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch_pointY(int)));

        connect(_side_widget->bezier_patch_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_patch_XYZ(int)));
        connect(_side_widget->bezier_patch_point_ID_X, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_patch_pointX_XYZ(int)));
        connect(_side_widget->bezier_patch_point_ID_Y, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_patch_pointY_XYZ(int)));
        connect(_side_widget->bezier_patch_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_patch_line_count(int)));

        connect(_side_widget->bezier_patch_point_X, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_bezier_patch_point_X(double)));
        connect(_side_widget->bezier_patch_point_Y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_bezier_patch_point_Y(double)));
        connect(_side_widget->bezier_patch_point_Z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_bezier_patch_point_Z(double)));

        connect(_side_widget->bezierp_control_point_scale, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_control_point_patch_scale(double)));
        connect(_side_widget->Control_Point_Visiblity_Patch,  SIGNAL(stateChanged(int)), _gl_widget, SLOT(setVisibilityOfPatchControlPoints(int)));
        connect(_side_widget->Control_Line_U_Visiblity,  SIGNAL(stateChanged(int)), _gl_widget, SLOT(setVisibilityOfPatchUControlLines(int)));
        connect(_side_widget->Control_Line_V_Visiblity,  SIGNAL(stateChanged(int)), _gl_widget, SLOT(setVisibilityOfPatchVControlLines(int)));
        connect(_side_widget->Normal_Vector_Visibility,  SIGNAL(stateChanged(int)), _gl_widget, SLOT(setVisibilityOfPatchNormalVectors(int)));
        connect(_side_widget->Control_Line_Count, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_control_line_count(int)));

        connect(_side_widget->add_curve_button, SIGNAL(clicked()), this, SLOT(add_curves()));
        connect(_side_widget->remove_curve_button, SIGNAL(clicked()), this, SLOT(delete_curves()));

        connect(_side_widget->merge_curve_button, SIGNAL(clicked()), this, SLOT(merge_curves()));
        connect(_side_widget->join_curve_button, SIGNAL(clicked()), this, SLOT(join_curves()));
        connect(_side_widget->extend_curve_button, SIGNAL(clicked()), this, SLOT(extend_curve()));

        connect(_side_widget->add_patch_button, SIGNAL(clicked()), this, SLOT(add_patch()));
        connect(_side_widget->remove_patch_button, SIGNAL(clicked()), this, SLOT(delete_patch()));

        connect(_side_widget->merge_patch_button, SIGNAL(clicked()), this, SLOT(merge_patches()));
        connect(_side_widget->join_patch_button, SIGNAL(clicked()), this, SLOT(join_patches()));
        connect(_side_widget->extend_patch_button, SIGNAL(clicked()), this, SLOT(extend_patch()));

        connect(_side_widget->curveBox, SIGNAL(clicked(bool)), this, SLOT(check_curve(bool)));
        connect(_side_widget->patchBox, SIGNAL(clicked(bool)), this, SLOT(check_patch(bool)));

        connect(_side_widget->patchTransButton, SIGNAL(clicked()), this, SLOT(translate_patch()));
        connect(_side_widget->wholeTransButton, SIGNAL(clicked()), this, SLOT(translate_whole()));
        connect(_side_widget->lightAmbientButton, SIGNAL(clicked()), this, SLOT(set_light_ambience()));
        connect(_side_widget->lightConstantButton, SIGNAL(clicked()), this, SLOT(set_light_constant()));
        connect(_side_widget->lightCutoffButton, SIGNAL(clicked()), this, SLOT(set_light_spot_cutoff()));
        connect(_side_widget->lightDiffuseButton, SIGNAL(clicked()), this, SLOT(set_light_diffuse()));
        connect(_side_widget->lightExponantButton, SIGNAL(clicked()), this, SLOT(set_light_spot_exponent()));
        connect(_side_widget->lightLinearButton, SIGNAL(clicked()), this, SLOT(set_light_linear()));
        connect(_side_widget->lightPosButton, SIGNAL(clicked()), this, SLOT(set_light_pos()));
        connect(_side_widget->lightQuadraticButton, SIGNAL(clicked()), this, SLOT(set_light_quadratic()));
        connect(_side_widget->lightSpecularButton, SIGNAL(clicked()), this, SLOT(set_light_specular()));
        connect(_side_widget->lightSpotDirButton, SIGNAL(clicked()), this, SLOT(set_light_spot_dir()));
        connect(_side_widget->bezier_patch_Light_Type_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(set_light_type(int)));

        connect(_side_widget->bezier_patch_Light_Type_ID, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_light_type(int)));
        connect(_side_widget->RColorSlider,  SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch_color_R(int)));
        connect(_side_widget->GColorSlider,  SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch_color_G(int)));
        connect(_side_widget->BColorSlider,  SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch_color_B(int)));
        connect(_side_widget->bezier_patch_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_patch_RGB(int)));

        connect(_side_widget->colorBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(setUseOfPatchColors(bool)));
        connect(_side_widget->shaderBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(setUseOfPatchShaders(bool)));
        connect(_side_widget->materialBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(setUseOfPatchMaterials(bool)));

        connect(_side_widget->bezier_patch_shader_A, SIGNAL(valueChanged(double)), this, SLOT(set_shader_param4(double)));
        connect(_side_widget->bezier_patch_shader_R, SIGNAL(valueChanged(double)), this, SLOT(set_shader_param1(double)));
        connect(_side_widget->bezier_patch_shader_G, SIGNAL(valueChanged(double)), this, SLOT(set_shader_param2(double)));
        connect(_side_widget->bezier_patch_shader_B, SIGNAL(valueChanged(double)), this, SLOT(set_shader_param3(double)));

        connect(_side_widget->bezier_patch_material, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_bezier_patch_material(int)));
        connect(_side_widget->bezier_patch_shader_type, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_selected_shader(int)));
        connect(_side_widget->bezier_patch_shader_type, SIGNAL(currentIndexChanged(int)), this, SLOT(set_shader_mods(int)));

        connect(_side_widget->Control_Line_Tangent_Visibility,  SIGNAL(stateChanged(int)), _gl_widget, SLOT(setVisibilityOfPatchTangents(int)));

        connect(_side_widget->RColorSlider_curve,  SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_selected_bezier_curve_color_R(int)));
        connect(_side_widget->GColorSlider_curve,  SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_selected_bezier_curve_color_G(int)));
        connect(_side_widget->BColorSlider_curve,  SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_selected_bezier_curve_color_B(int)));
        connect(_side_widget->bezier_curve_ID, SIGNAL(currentIndexChanged(int)), this, SLOT(update_bezier_curve_RGB(int)));

        connect(_side_widget->curveSaveButton, SIGNAL(clicked()), this, SLOT(save_curves()));
        connect(_side_widget->curveLoadButton, SIGNAL(clicked()), this, SLOT(load_curves()));
        connect(_side_widget->patchSaveButton, SIGNAL(clicked()), this, SLOT(save_patches()));
        connect(_side_widget->patchLoadButton, SIGNAL(clicked()), this, SLOT(load_patches()));
    };
    //--------------------------------
    // implementation of private slots
    //--------------------------------
    void MainWindow::on_action_Quit_triggered()
    {
        qApp->exit(0);
    }

    void MainWindow::update_bezier_curve_XYZ(int selectedCurve)
    {
        _side_widget->bezier_curve_point_X->setValue(_gl_widget->_get_bezier_X(selectedCurve,_side_widget->bezier_curve_point_ID->currentIndex()));
        _side_widget->bezier_curve_point_Y->setValue(_gl_widget->_get_bezier_Y(selectedCurve,_side_widget->bezier_curve_point_ID->currentIndex()));
        _side_widget->bezier_curve_point_Z->setValue(_gl_widget->_get_bezier_Z(selectedCurve,_side_widget->bezier_curve_point_ID->currentIndex()));
    }

    void MainWindow::update_bezier_curve_point_XYZ(int selectedPoint)
    {
        _side_widget->bezier_curve_point_X->setValue(_gl_widget->_get_bezier_X(_side_widget->bezier_curve_ID->currentIndex(),selectedPoint));
        _side_widget->bezier_curve_point_Y->setValue(_gl_widget->_get_bezier_Y(_side_widget->bezier_curve_ID->currentIndex(),selectedPoint));
        _side_widget->bezier_curve_point_Z->setValue(_gl_widget->_get_bezier_Z(_side_widget->bezier_curve_ID->currentIndex(),selectedPoint));
    }

    void MainWindow::update_bezier_patch_XYZ(int selectedCurve)
    {
        _side_widget->bezier_patch_point_X->setValue(_gl_widget->_get_bezierp_X(selectedCurve,_side_widget->bezier_patch_point_ID_X->currentIndex(),_side_widget->bezier_patch_point_ID_Y->currentIndex()));
        _side_widget->bezier_patch_point_Y->setValue(_gl_widget->_get_bezierp_Y(selectedCurve,_side_widget->bezier_patch_point_ID_X->currentIndex(),_side_widget->bezier_patch_point_ID_Y->currentIndex()));
        _side_widget->bezier_patch_point_Z->setValue(_gl_widget->_get_bezierp_Z(selectedCurve,_side_widget->bezier_patch_point_ID_X->currentIndex(),_side_widget->bezier_patch_point_ID_Y->currentIndex()));
    }

    void MainWindow::update_bezier_patch_pointX_XYZ(int selectedPoint)
    {
        _side_widget->bezier_patch_point_X->setValue(_gl_widget->_get_bezierp_X(_side_widget->bezier_curve_ID->currentIndex(),selectedPoint,_side_widget->bezier_patch_point_ID_Y->currentIndex()));
        _side_widget->bezier_patch_point_Y->setValue(_gl_widget->_get_bezierp_Y(_side_widget->bezier_curve_ID->currentIndex(),selectedPoint,_side_widget->bezier_patch_point_ID_Y->currentIndex()));
        _side_widget->bezier_patch_point_Z->setValue(_gl_widget->_get_bezierp_Z(_side_widget->bezier_curve_ID->currentIndex(),selectedPoint,_side_widget->bezier_patch_point_ID_Y->currentIndex()));
    }

    void MainWindow::update_bezier_patch_pointY_XYZ(int selectedPoint)
    {
        _side_widget->bezier_patch_point_X->setValue(_gl_widget->_get_bezierp_X(_side_widget->bezier_curve_ID->currentIndex(),_side_widget->bezier_patch_point_ID_X->currentIndex(),selectedPoint));
        _side_widget->bezier_patch_point_Y->setValue(_gl_widget->_get_bezierp_Y(_side_widget->bezier_curve_ID->currentIndex(),_side_widget->bezier_patch_point_ID_X->currentIndex(),selectedPoint));
        _side_widget->bezier_patch_point_Z->setValue(_gl_widget->_get_bezierp_Z(_side_widget->bezier_curve_ID->currentIndex(),_side_widget->bezier_patch_point_ID_X->currentIndex(),selectedPoint));
    }

    void MainWindow::update_bezier_patch_line_count(int selectedCurve)
    {
        _side_widget->Control_Line_Count->setValue(_gl_widget->_get_bezier_line_count(selectedCurve));
    }

    void MainWindow::merge_curves()
    {
        if (!_gl_widget->mergeCurves(_side_widget->bezier_curve_other_ID->currentIndex(),
                                _side_widget->bezier_curve_side->currentIndex(),
                                _side_widget->bezier_curve_other_side->currentIndex()))
        {
            _side_widget->curve_Error->setHidden(false);
            _side_widget->curve_Error->setText("Couldn't merge the two curves,\n check if both sides are free!");
            return;
        }
        _side_widget->curve_Error->setHidden(true);
    }

    void MainWindow::extend_curve()
    {
        if (!_gl_widget->extendCurve(_side_widget->bezier_curve_side->currentIndex()))
        {
            _side_widget->curve_Error->setHidden(false);
            _side_widget->curve_Error->setText("Couldn't extend the curve, \n check if the given side is free!");
            return;
        }
        _side_widget->curve_Error->setHidden(true);
        QString last = _side_widget->bezier_curve_ID->itemText(_side_widget->bezier_curve_ID->count() - 1);
        _side_widget->bezier_curve_ID->addItem(QString::number(last.toInt() + 1));
        _side_widget->bezier_curve_other_ID->addItem(QString::number(last.toInt() + 1));
    }

    void MainWindow::add_curves()
    {
        _gl_widget->addCurve();
        QString last = _side_widget->bezier_curve_ID->itemText(_side_widget->bezier_curve_ID->count() - 1);
        _side_widget->bezier_curve_ID->addItem(QString::number(last.toInt() + 1));
        _side_widget->bezier_curve_other_ID->addItem(QString::number(last.toInt() + 1));
    }

    void MainWindow::delete_curves()
    {
        _gl_widget->deleteCurve();
        _side_widget->bezier_curve_ID->removeItem(_side_widget->bezier_curve_ID->currentIndex());
        _side_widget->bezier_curve_other_ID->removeItem(_side_widget->bezier_curve_ID->currentIndex());
    }

    void MainWindow::merge_patches()
    {
        if (!_gl_widget->mergePatches(_side_widget->bezier_patch_other_ID->currentIndex(),
                                _side_widget->bezier_patch_side->currentIndex(),
                                _side_widget->bezier_patch_other_side->currentIndex()))
        {
            _side_widget->patch_Error->setHidden(false);
            _side_widget->patch_Error->setText("Couldn't merge the two patches,\n check if both sides are free!");
            return;
        }
        _side_widget->patch_Error->setHidden(true);
    }

    void MainWindow::extend_patch()
    {
        if (!_gl_widget->extendPatch(_side_widget->bezier_patch_side->currentIndex()))
        {
            _side_widget->patch_Error->setHidden(false);
            _side_widget->patch_Error->setText("Couldn't extend the curve, \n check if the given side is free!");
            return;
        }
        _side_widget->patch_Error->setHidden(true);
        QString last = _side_widget->bezier_patch_ID->itemText(_side_widget->bezier_patch_ID->count() - 1);
        _side_widget->bezier_patch_ID->addItem(QString::number(last.toInt() + 1));
        _side_widget->bezier_patch_other_ID->addItem(QString::number(last.toInt() + 1));
    }

    void MainWindow::add_patch()
    {
        _gl_widget->addPatch();
        QString last = _side_widget->bezier_patch_ID->itemText(_side_widget->bezier_patch_ID->count() - 1);
        _side_widget->bezier_patch_ID->addItem(QString::number(last.toInt() + 1));
        _side_widget->bezier_patch_other_ID->addItem(QString::number(last.toInt() + 1));
    }

    void MainWindow::delete_patch()
    {
        _gl_widget->deletePatch();
        _side_widget->bezier_patch_other_ID->removeItem(_side_widget->bezier_patch_ID->currentIndex());
        _side_widget->bezier_patch_ID->removeItem(_side_widget->bezier_patch_ID->currentIndex());
    }

    void MainWindow::check_curve(bool change)
    {
        if (change)
        {
            _gl_widget->switch_curves_patches(false);
            _side_widget->patchBox->setChecked(false);
        }
        else
        {
            _gl_widget->switch_curves_patches(true);
            _side_widget->patchBox->setChecked(true);
        }
    }

    void MainWindow::check_patch(bool change)
    {
        if (change)
        {
            _gl_widget->switch_curves_patches(true);
            _side_widget->curveBox->setChecked(false);
        }
        else
        {
            _gl_widget->switch_curves_patches(false);
            _side_widget->curveBox->setChecked(true);
        }
    }

    void MainWindow::translate_patch()
    {
        _gl_widget->translate_patch(_side_widget->bezier_patch_trans_X->value(),
                                    _side_widget->bezier_patch_trans_Y->value(),
                                    _side_widget->bezier_patch_trans_Z->value());
    }

    void MainWindow::translate_whole()
    {
        _gl_widget->translate_whole(_side_widget->bezier_patch_trans_X->value(),
                                    _side_widget->bezier_patch_trans_Y->value(),
                                    _side_widget->bezier_patch_trans_Z->value());
    }

    void MainWindow::set_light_type(int id)
    {
        if (id == 0)
        {
            _side_widget->lightConstantButton->setEnabled(false);
            _side_widget->lightLinearButton->setEnabled(false);
            _side_widget->lightQuadraticButton->setEnabled(false);
            _side_widget->lightSpotDirButton->setEnabled(false);
            _side_widget->lightCutoffButton->setEnabled(false);
            _side_widget->lightExponantButton->setEnabled(false);
        }
        else if (id == 1)
        {
            _side_widget->lightConstantButton->setEnabled(true);
            _side_widget->lightLinearButton->setEnabled(true);
            _side_widget->lightQuadraticButton->setEnabled(true);
            _side_widget->lightSpotDirButton->setEnabled(false);
            _side_widget->lightCutoffButton->setEnabled(false);
            _side_widget->lightExponantButton->setEnabled(false);
        }
        else
        {
            _side_widget->lightConstantButton->setEnabled(true);
            _side_widget->lightLinearButton->setEnabled(true);
            _side_widget->lightQuadraticButton->setEnabled(true);
            _side_widget->lightSpotDirButton->setEnabled(true);
            _side_widget->lightCutoffButton->setEnabled(true);
            _side_widget->lightExponantButton->setEnabled(true);
        }
    }

    void MainWindow::set_light_pos()
    {
        _gl_widget->set_light_pos(_side_widget->bezier_patch_light_X->value(),
                                  _side_widget->bezier_patch_light_Y->value(),
                                  _side_widget->bezier_patch_light_Z->value(),
                                  _side_widget->bezier_patch_light_W->value());
    }

    void MainWindow::set_light_ambience()
    {
        _gl_widget->set_light_ambience(_side_widget->bezier_patch_light_X->value(),
                                  _side_widget->bezier_patch_light_Y->value(),
                                  _side_widget->bezier_patch_light_Z->value(),
                                  _side_widget->bezier_patch_light_W->value());
    }
    void MainWindow::set_light_diffuse()
    {
        _gl_widget->set_light_diffuse(_side_widget->bezier_patch_light_X->value(),
                                  _side_widget->bezier_patch_light_Y->value(),
                                  _side_widget->bezier_patch_light_Z->value(),
                                  _side_widget->bezier_patch_light_W->value());
    }
    void MainWindow::set_light_specular()
    {
        _gl_widget->set_light_specular(_side_widget->bezier_patch_light_X->value(),
                                  _side_widget->bezier_patch_light_Y->value(),
                                  _side_widget->bezier_patch_light_Z->value(),
                                  _side_widget->bezier_patch_light_W->value());
    }
    void MainWindow::set_light_constant()
    {
        _gl_widget->set_light_constant(_side_widget->bezier_patch_light_other->value());
    }
    void MainWindow::set_light_linear()
    {
        _gl_widget->set_light_linear(_side_widget->bezier_patch_light_other->value());
    }
    void MainWindow::set_light_quadratic()
    {
        _gl_widget->set_light_quadratic(_side_widget->bezier_patch_light_other->value());
    }
    void MainWindow::set_light_spot_dir()
    {
        _gl_widget->set_light_spot_dir(_side_widget->bezier_patch_light_X->value(),
                                  _side_widget->bezier_patch_light_Y->value(),
                                  _side_widget->bezier_patch_light_Z->value());
    }
    void MainWindow::set_light_spot_cutoff()
    {
        _gl_widget->set_light_spot_cutoff(_side_widget->bezier_patch_light_other->value());
    }
    void MainWindow::set_light_spot_exponent()
    {
        _gl_widget->set_light_spot_exponent(_side_widget->bezier_patch_light_other->value());
    }

    void MainWindow::update_bezier_patch_RGB(int)
    {
        _side_widget->RColorSlider->setValue(_gl_widget->get_selected_bezier_patch_color_R());
        _side_widget->GColorSlider->setValue(_gl_widget->get_selected_bezier_patch_color_G());
        _side_widget->GColorSlider->setValue(_gl_widget->get_selected_bezier_patch_color_B());
    }

    void MainWindow::join_patches()
    {
        if (!_gl_widget->jointPatches(_side_widget->bezier_patch_other_ID->currentIndex(),
                                      _side_widget->bezier_patch_side->currentIndex(),
                                      _side_widget->bezier_patch_other_side->currentIndex()))
        {
            _side_widget->patch_Error->setHidden(false);
            _side_widget->patch_Error->setText("Couldn't join the curves, \n check if the given side is free!");
            return;
        }
        _side_widget->patch_Error->setHidden(true);
        QString last = _side_widget->bezier_patch_ID->itemText(_side_widget->bezier_patch_ID->count() - 1);
        _side_widget->bezier_patch_ID->addItem(QString::number(last.toInt() + 1));
        _side_widget->bezier_patch_other_ID->addItem(QString::number(last.toInt() + 1));
    }

    void MainWindow::join_curves()
    {
        if (!_gl_widget->joinCurves(_side_widget->bezier_curve_other_ID->currentIndex(),
                                    _side_widget->bezier_curve_side->currentIndex(),
                                    _side_widget->bezier_curve_other_side->currentIndex()))
        {
            _side_widget->curve_Error->setHidden(false);
            _side_widget->curve_Error->setText("Couldn't join the curves, \n check if the given side is free!");
            return;
        }
        _side_widget->curve_Error->setHidden(true);
        QString last = _side_widget->bezier_curve_ID->itemText(_side_widget->bezier_curve_ID->count() - 1);
        _side_widget->bezier_curve_ID->addItem(QString::number(last.toInt() + 1));
        _side_widget->bezier_curve_other_ID->addItem(QString::number(last.toInt() + 1));
    }

    void MainWindow::set_shader_param1(double val)
    {
        if (_side_widget->bezier_patch_shader_type->currentIndex() == 1)
        {
            _gl_widget->set_refl_scale(val);
        }
        else
        {
            _gl_widget->set_toon_color_R(val);
        }
    }
    void MainWindow::set_shader_param2(double val)
    {
        if (_side_widget->bezier_patch_shader_type->currentIndex() == 1)
        {
            _gl_widget->set_refl_smoothing(val);
        }
        else
        {
            _gl_widget->set_toon_color_G(val);
        }
    }
    void MainWindow::set_shader_param3(double val)
    {
        if (_side_widget->bezier_patch_shader_type->currentIndex() == 1)
        {
            _gl_widget->set_refl_shading(val);
        }
        else
        {
            _gl_widget->set_toon_color_B(val);
        }
    }
    void MainWindow::set_shader_param4(double val)
    {
        _gl_widget->set_toon_color_A(val);
    }

    void MainWindow::set_shader_mods(int ind)
    {
        switch (ind) {
        case 1:
            _side_widget->bezier_patch_shader_R->setDisabled(false);
            _side_widget->bezier_patch_shader_G->setDisabled(false);
            _side_widget->bezier_patch_shader_B->setDisabled(false);
            _side_widget->bezier_patch_shader_A->setDisabled(true);
            _side_widget->bezier_patch_shader_R_label->setHidden(false);
            _side_widget->bezier_patch_shader_R_label->setText("Scale Factor");
            _side_widget->bezier_patch_shader_G_label->setHidden(false);
            _side_widget->bezier_patch_shader_G_label->setText("Smoothing");
            _side_widget->bezier_patch_shader_B_label->setHidden(false);
            _side_widget->bezier_patch_shader_B_label->setText("Shading");
            _side_widget->bezier_patch_shader_A_label->setHidden(true);
            break;
        case 2:
            _side_widget->bezier_patch_shader_R->setDisabled(false);
            _side_widget->bezier_patch_shader_G->setDisabled(false);
            _side_widget->bezier_patch_shader_B->setDisabled(false);
            _side_widget->bezier_patch_shader_A->setDisabled(false);
            _side_widget->bezier_patch_shader_R_label->setHidden(false);
            _side_widget->bezier_patch_shader_R_label->setText("R");
            _side_widget->bezier_patch_shader_G_label->setHidden(false);
            _side_widget->bezier_patch_shader_G_label->setText("G");
            _side_widget->bezier_patch_shader_B_label->setHidden(false);
            _side_widget->bezier_patch_shader_B_label->setText("B");
            _side_widget->bezier_patch_shader_A_label->setHidden(false);
            break;
        default:
            _side_widget->bezier_patch_shader_R->setDisabled(true);
            _side_widget->bezier_patch_shader_G->setDisabled(true);
            _side_widget->bezier_patch_shader_B->setDisabled(true);
            _side_widget->bezier_patch_shader_A->setDisabled(true);
            _side_widget->bezier_patch_shader_R_label->setHidden(true);
            _side_widget->bezier_patch_shader_G_label->setHidden(true);
            _side_widget->bezier_patch_shader_B_label->setHidden(true);
            _side_widget->bezier_patch_shader_A_label->setHidden(true);
        }
    }

    void MainWindow::update_bezier_curve_RGB(int)
    {
        _side_widget->RColorSlider_curve->setValue(_gl_widget->get_selected_bezier_curve_color_R());
        _side_widget->GColorSlider_curve->setValue(_gl_widget->get_selected_bezier_curve_color_G());
        _side_widget->GColorSlider_curve->setValue(_gl_widget->get_selected_bezier_curve_color_B());
    }

    void MainWindow::save_curves()
    {
        _gl_widget->saveCurves();
    }

    void MainWindow::load_curves()
    {
        _gl_widget->loadCurves();
        _side_widget->bezier_curve_ID->clear();
        int n = _gl_widget->get_bezierc_count();
        for (int i = _side_widget->bezier_curve_ID->count(); i<n; i++)
        {
            QString last = _side_widget->bezier_curve_ID->itemText(_side_widget->bezier_curve_ID->count() - 1);
            _side_widget->bezier_curve_ID->addItem(QString::number(last.toInt() + 1));
            _side_widget->bezier_curve_other_ID->addItem(QString::number(last.toInt() + 1));
        }
        for (int i = n; i<_side_widget->bezier_curve_ID->count(); i++)
        {
            _side_widget->bezier_curve_ID->removeItem(_side_widget->bezier_curve_ID->count() - 1);
            _side_widget->bezier_curve_other_ID->removeItem(_side_widget->bezier_curve_other_ID->count() - 1);
        }
        _side_widget->bezier_curve_ID->setCurrentIndex(0);
    }

    void MainWindow::save_patches()
    {
        _gl_widget->savePatches();
    }

    void MainWindow::load_patches()
    {
        _gl_widget->loadPatches();
        int n = _gl_widget->get_bezierp_count();
        std::cout << "this " << n;
        for (int i = _side_widget->bezier_patch_ID->count(); i<n; i++)
        {
            QString last = _side_widget->bezier_patch_ID->itemText(_side_widget->bezier_patch_ID->count() - 1);
            _side_widget->bezier_patch_ID->addItem(QString::number(last.toInt() + 1));
            _side_widget->bezier_patch_other_ID->addItem(QString::number(last.toInt() + 1));
        }

        for (int i = n; i<_side_widget->bezier_patch_ID->count(); i++)
        {
            _side_widget->bezier_patch_ID->removeItem(_side_widget->bezier_patch_ID->count() - 1);
            _side_widget->bezier_patch_other_ID->removeItem(_side_widget->bezier_patch_other_ID->count() - 1);
        }
        _side_widget->bezier_patch_ID->setCurrentIndex(0);
    }
}
