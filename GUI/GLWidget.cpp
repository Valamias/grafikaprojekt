#include "GLWidget.h"

#if !defined(__APPLE__)
#include <GL/glu.h>
#endif

#include <iostream>
#include <fstream>
#include <QMouseEvent>
#include <QWheelEvent>
using namespace std;

#include "../Core/Exceptions.h"
#include "../Core/DCoordinates3.h"
#include "../Core/Matrices.h"
#include "../Core/RealSquareMatrices.h"
#include "../Core/GenericCurves3.h"
#include "../Parametric/ParametricCurves3.h"
#include "../Cyclic/CyclicCurves3.h"
#include "../Test/TestFunctions.h"
#include "../Core/Constants.h"
#include "../Parametric/ParametricSurfaces3.h"
#include "../Core/ShaderPrograms.h"

enum curve_sides{CURVE_LEFT=0, CURVE_RIGHT=1};
enum patchSides{PATCH_LEFT=0, PATCH_UP=1, PATCH_RIGHT=2, PATCH_DOWN=3};

namespace cagd
{
    bool GLWidget::_createAllBezierCurvesAndTheirImages()
    {
        _bezier_count = 9;
        _bezierc.resize(_bezier_count);
        _bezier_points.ResizeColumns(_bezier_count);
        _bezier_control_point_obj.LoadFromOFF("Models\\Building blocks\\Primitives\\sphere.off",GL_TRUE);
        _bezier_control_point_obj.UpdateVertexBufferObjects();

        for (GLuint k = 0; k < _bezier_count; k++)
        {
            _bezier_points[k].ResizeRows(4);

            for (GLuint i = 0; i < 4; i++)
            {
                _bezier_points[k][i] = DCoordinate3(i + 6*(k/3) + 2*(i == 1) - 2*(i == 2),i + 6*(k%3),0);
            }

            _bezierc[k] = new CubicBezierCurve(_bezier_points[k]);
            _bezierc[k]->setColorR(((double)k/3));
            _bezierc[k]->setColorG(1. - ((double)k/3));
            _bezierc[k]->setColorB(0.3 * (k%3));
        }

//
//        _bezier_points.ResizeColumns(_bezier_count);
//        _bezier_control_point_obj.LoadFromOFF("Models\\Building blocks\\Primitives\\sphere.off",GL_TRUE);
//        _bezier_control_point_obj.UpdateVertexBufferObjects();

//        _bezier_points[0].ResizeRows(4);
//        _bezier_points[0][0] = DCoordinate3();
//        _bezier_points[0][1] = DCoordinate3(1,2,3);
//        _bezier_points[0][2] = DCoordinate3(3,2,1);
//        _bezier_points[0][3] = DCoordinate3(1,1,1);
//        _bezierc[0] = new CubicBezierCurve(_bezier_points[0]);

//        _bezier_points[1].ResizeRows(4);
//        _bezier_points[1][0] = DCoordinate3();
//        _bezier_points[1][1] = DCoordinate3(-1,-2,-3);
//        _bezier_points[1][2] = DCoordinate3(4,3,2);
//        _bezier_points[1][3] = DCoordinate3(2,2,2);
//        _bezierc[1] = new CubicBezierCurve(_bezier_points[1]);
//        _bezierc[0]->attach(CURVE_LEFT, CURVE_LEFT, _bezierc[1]);
//        _bezierc[1]->attach(CURVE_LEFT, CURVE_LEFT, _bezierc[0]);

        // a képek és azok csúcspont-pufferobjektumainak egységes létrehozása
        for (GLuint i = 0; i < _bezier_count; i++)
        {
            _bezierc[i]->setDivPointCount(_bezier_div_point_count);

            if (!_bezierc[i]->updateImageForRendering())
            {
                _destroyAllExistingBezierCurvesAndTheirImages();
                return false;
            }
        }
        return true;
    }

    void GLWidget::_destroyAllExistingBezierCurvesAndTheirImages()
    {
        for (GLuint i = 0; i < _bezier_count; i++)
        {
             if (_bezierc[i])
             {
                 delete _bezierc[i]; _bezierc[i] = nullptr;
             }
        }

    }

    bool GLWidget::_renderAllBezierCurves()
    {
        for(GLuint i=0; i < _bezier_count; i++)
        {

            glColor3d(_bezierc[i]->getColorR(),
                      _bezierc[i]->getColorG(),
                      _bezierc[i]->getColorB());
            _bezierc[i]->render(0,GL_LINE_STRIP);

            if (_show_control_points && i == _selected_bezier_curve)
            {
                for (GLuint j=0; j<4; j++)
                {
                    glPushMatrix();
                            switch(j)
                            {
                               case 0:
                                glColor3f(1.0f,0.0f,0.0f);
                                break;
                               case 1:
                                glColor3f(0.5f,0.0f,0.0f);
                                break;
                               case 2:
                                glColor3f(0.0f,0.0f,0.5f);
                                break;
                               case 3:
                                glColor3f(0.0f,0.0f,1.0f);
                                break;
                            }
                            if (j == _selected_bezier_curve_point)
                                glColor3f(0.0f,1.0f,0.0f);
                            glTranslated(_bezierc[i]->getPointCoordinate(j).x(),_bezierc[i]->getPointCoordinate(j).y(),_bezierc[i]->getPointCoordinate(j).z());
                            glScaled(_control_scale, _control_scale, _control_scale);
                            _bezier_control_point_obj.Render();
                     glPopMatrix();
                }
                _bezierc[i]->RenderData();
            }

            if (_show_tangents)
            {
                glColor3f(1.0f,1.0f,0.0f);
                _bezierc[i]->render(1,GL_LINES);
            }

            if (_show_acceleration_vectors)
            {
                glColor3f(0.0f,1.0f,1.0f);
                _bezierc[i]->render(2,GL_LINES);
            }
        }

        return true;
    }

    bool GLWidget::_updateSelectedBezierCurve()
    {
        if (!_bezierc[_selected_bezier_curve]->updateImageForRendering())
        {
            std::cout << "Couln't update curve vertexbuffer objects!" << std::endl;
            return false;
        }
        return true;
    }

    bool GLWidget::_updateAllBezierCurves()
    {
        for (GLuint i=0; i<_bezier_count; i++)
        {
            if (!_bezierc[i]->updateImageForRendering())
            {
                std::cout << "Couln't update curve vertexbuffer objects!" << std::endl;
                return false;
            }
        }
        return true;
    }

    GLdouble GLWidget::_get_bezier_X(int selected_curve, int selected_point)
    {
        return _bezierc[_selected_bezier_curve]->getPointCoordinate(_selected_bezier_curve_point).x();
    }

    GLdouble GLWidget::_get_bezier_Y(int selected_curve,int selected_point)
    {
        return _bezierc[_selected_bezier_curve]->getPointCoordinate(_selected_bezier_curve_point).y();
    }

    GLdouble GLWidget::_get_bezier_Z(int selected_curve,int selected_point)
    {
        return _bezierc[_selected_bezier_curve]->getPointCoordinate(_selected_bezier_curve_point).z();
    }

    GLuint GLWidget::_get_bezier_line_count(int selected_curve)
    {
        return _bezierp[selected_curve]->get_u_line_count();
    }

    bool GLWidget::_createAllBezierPatchesAndTheirImages()
    {
        _bezierp_count = 9;
        _bezierp.resize(_bezierp_count);
        _bezierp_points.ResizeColumns(_bezierp_count);

        for (GLuint k = 0; k < _bezierp_count; k++)
        {
            _bezierp[k] = new BicubicBezierPatch();
            for (GLuint i = 0; i < 4; i++)
            {
                for (GLuint j = 0; j < 4; j++)
                {
                    _bezierp[k]->SetData(i,j,DCoordinate3(i + 6*(k/3),j + 6*(k%3),0));
                }
            }
            _bezierp[k]->setColorR(((double)k/3));
            _bezierp[k]->setColorG(1. - ((double)k/3));
            _bezierp[k]->setColorB(0.3 * (k%3));
        }
//        _bezierp_points[0].ResizeRows(4);
//        _bezierp_points[0][0].ResizeColumns(4);
//        _bezierp_points[0][0][0]  = DCoordinate3(0,0);
//        _bezierp_points[0][0][1]  = DCoordinate3(1,0);
//        _bezierp_points[0][0][2]  = DCoordinate3(2,0);
//        _bezierp_points[0][0][3]  = DCoordinate3(3,0);
//        _bezierp_points[0][1].ResizeColumns(4);
//        _bezierp_points[0][1][0]  = DCoordinate3(0,1);
//        _bezierp_points[0][1][1]  = DCoordinate3(1,1);
//        _bezierp_points[0][1][2]  = DCoordinate3(2,1);
//        _bezierp_points[0][1][3]  = DCoordinate3(3,1);
//        _bezierp_points[0][2].ResizeColumns(4);
//        _bezierp_points[0][2][0]  = DCoordinate3(0,2);
//        _bezierp_points[0][2][1]  = DCoordinate3(1,2);
//        _bezierp_points[0][2][2] = DCoordinate3(2,2);
//        _bezierp_points[0][2][3] = DCoordinate3(3,2);
//        _bezierp_points[0][3].ResizeColumns(4);
//        _bezierp_points[0][3][0] = DCoordinate3(0,3);
//        _bezierp_points[0][3][1] = DCoordinate3(1,3);
//        _bezierp_points[0][3][2] = DCoordinate3(2,3);
//        _bezierp_points[0][3][3] = DCoordinate3(3,3);

//        _bezierp[0] = new BicubicBezierPatch();
//        for (GLuint i = 0; i < 4; i++)
//        {
//            for (GLuint j = 0; j < 4; j++)
//            {
//                _bezierp[0]->SetData(i,j,_bezierp_points[0][i][j]);
//            }
//        }

//        _bezierp_points[1].ResizeRows(4);
//        _bezierp_points[1][0].ResizeColumns(4);
//        _bezierp_points[1][0][0]  = DCoordinate3(0,3);
//        _bezierp_points[1][0][1]  = DCoordinate3(1,3);
//        _bezierp_points[1][0][2]  = DCoordinate3(2,3);
//        _bezierp_points[1][0][3]  = DCoordinate3(3,3);
//        _bezierp_points[1][1].ResizeColumns(4);
//        _bezierp_points[1][1][0]  = DCoordinate3(0,4);
//        _bezierp_points[1][1][1]  = DCoordinate3(1,4);
//        _bezierp_points[1][1][2]  = DCoordinate3(2,4);
//        _bezierp_points[1][1][3]  = DCoordinate3(3,4);
//        _bezierp_points[1][2].ResizeColumns(4);
//        _bezierp_points[1][2][0]  = DCoordinate3(0,5);
//        _bezierp_points[1][2][1]  = DCoordinate3(1,5);
//        _bezierp_points[1][2][2] = DCoordinate3(2,5);
//        _bezierp_points[1][2][3] = DCoordinate3(3,5);
//        _bezierp_points[1][3].ResizeColumns(4);
//        _bezierp_points[1][3][0] = DCoordinate3(0,6);
//        _bezierp_points[1][3][1] = DCoordinate3(1,6);
//        _bezierp_points[1][3][2] = DCoordinate3(2,6);
//        _bezierp_points[1][3][3] = DCoordinate3(3,6);

//        _bezierp[1] = new BicubicBezierPatch();

//        for (GLuint i = 0; i < 4; i++)
//        {
//            for (GLuint j = 0; j < 4; j++)
//            {
//                _bezierp[1]->SetData(i,j,_bezierp_points[1][i][j]);
//            }
//        }

//        _bezierp[0]->attach(PATCH_DOWN, PATCH_UP, _bezierp[1]);
//        _bezierp[1]->attach(PATCH_UP, PATCH_DOWN, _bezierp[0]);

        // a képek és azok csúcspont-pufferobjektumainak egységes létrehozása
        for (GLuint i = 0; i < _bezierp_count; i++)
        {
            _bezierp[i]->set_u_line_count(4);
            _bezierp[i]->set_v_line_count(4);

            _bezierp[i]->set_u_div_point_count(_bezierp_div_point_count_u);
            _bezierp[i]->set_v_div_point_count(_bezierp_div_point_count_v);

            if (!_bezierp[i]->updateImageForRendering())
            {
                _destroyAllExistingBezierPatchesAndTheirImages();
                return false;
            }
        }
        return true;
    }

    void GLWidget::_destroyAllExistingBezierPatchesAndTheirImages()
    {
        for (GLuint i = 0; i < _bezierp_count; i++)
        {
             if (_bezierp[i])
             {
                 delete _bezierp[i]; _bezierp[i] = nullptr;
             }
        }
    }

    bool GLWidget::_renderAllBezierPatches()
    {
        for(GLuint i=0; i < _bezierp_count; i++)
        {
            _shaders[1]->Enable();

            _shaders[1]->Disable();

            _shaders[2]->Enable();
            _shaders[2]->Disable();
            if (_use_shaders)
            {
                switch(_bezierp[i]->get_selected_shader())
                {
                case 0:
                    _shaders[0]->Enable();
                    break;
                case 1:
                    _shaders[1]->Enable();
                    _shaders[1]->SetUniformVariable1f("scale_factor", _bezierp[i]->get_refl_scale());
                    _shaders[1]->SetUniformVariable1f("smoothing", _bezierp[i]->get_refl_smoothing());
                    _shaders[1]->SetUniformVariable1f("shading", _bezierp[i]->get_refl_shading());
                    break;
                case 2:
                    _shaders[2]->Enable();
                    _shaders[2]->SetUniformVariable4f("default_outline_color", _bezierp[i]->get_toon_color_R(),
                                                        _bezierp[i]->get_toon_color_G(),
                                                        _bezierp[i]->get_toon_color_B(),
                                                        _bezierp[i]->get_toon_color_A());
                    break;
                case 3:
                    _shaders[3]->Enable();
                }

                _bezierp[i]->render();
                _shaders[_bezierp[i]->get_selected_shader()]->Disable();
            }
            else
            {

                glPushMatrix();
                        if (_use_colors)
                        {
                            glColor3f(_bezierp[i]->getColorR(),
                                      _bezierp[i]->getColorG(),
                                      _bezierp[i]->getColorB());
                            _bezierp[i]->render();
                        }
                        else
                        {
                            glEnable(GL_LIGHTING);
                            glEnable(GL_NORMALIZE);

                            switch(_selected_light)
                            {
                            case 0:
                                _Dir_light = DirectionalLight(
                                            _light_index[0],
                                            _light_position[0],
                                            _light_ambient_intensity[0],
                                            _light_diffuse_intensity[0],
                                            _light_specular_intensity[0]
                                            );
                                _Dir_light.Enable();
                                break;
                            case 1:
                                _Point_light = PointLight(
                                            _light_index[1],
                                            _light_position[1],
                                            _light_ambient_intensity[1],
                                            _light_diffuse_intensity[1],
                                            _light_specular_intensity[1],
                                            _light_constant_attenuation[1],
                                            _light_linear_attenuation[1],
                                            _light_quadratic_attenuation[1]
                                            );
                                _Point_light.Enable();
                                break;
                            case 2:
                                _Spot_light = Spotlight(
                                            _light_index[2],
                                            _light_position[2],
                                            _light_ambient_intensity[2],
                                            _light_diffuse_intensity[2],
                                            _light_specular_intensity[2],
                                            _light_constant_attenuation[2],
                                            _light_linear_attenuation[2],
                                            _light_quadratic_attenuation[2],
                                            _light_spot_direction[2],
                                            _light_spot_cutoff[2],
                                            _light_spot_exponent[2]
                                            );
                                _Spot_light.Enable();
                                break;
                            }

                            if (_use_materials)
                                _materials[_bezierp[i]->getMaterial()]->Apply();

                            _bezierp[i]->render();
                            switch(_selected_light)
                            {
                            case 0:
                                _Dir_light.Disable();
                                break;
                            case 1:
                                _Point_light.Disable();
                                break;
                            case 2:
                                _Spot_light.Disable();
                                break;
                            }
                            glDisable(GL_NORMALIZE);
                            glDisable(GL_LIGHTING);
                        }
                 glPopMatrix();
            }


            if (_show_patch_control_points && i == _selected_bezier_patch)
            {
                _bezierp[i]->RenderData();
                for (GLuint j=0; j<4; j++)
                {
                    for (GLuint k=0; k<4; k++)
                    {
                        glPushMatrix();
                                if ((j == 0 || j == 3) && (k == 0 || k == 3))
                                    glColor3f(1.0f,0.0f,0.0f);
                                else if ((j == 1 || j == 2) && (k == 1 || k == 2))
                                    glColor3f(0.5f,0.0f,0.0f);
                                else
                                {
                                    if (k == 0)
                                        glColor3f(0.0f,0.0f,1.f);
                                    if (k == 3)
                                        glColor3f(0.0f,1.f,1.f);
                                    if (j == 0)
                                        glColor3f(1.f,0.0f,1.f);
                                    if (j == 3)
                                        glColor3f(1.f,1.f,0.0f);
                                }
                                if (j == _selected_bezier_patch_pointX && k == _selected_bezier_patch_pointY)
                                    glColor3f(0.0f,1.0f,0.0f);

                                glTranslated((*_bezierp[i])(j,k).x(),(*_bezierp[i])(j,k).y(),(*_bezierp[i])(j,k).z());
                                glScaled(_control_scale_patch, _control_scale_patch, _control_scale_patch);
                                _bezier_control_point_obj.Render();
                         glPopMatrix();
                    }
                }
            }
            if (_show_U_control_lines  && i == _selected_bezier_patch)
            {
                glColor3f(1.0f,0.0f,0.0f);
                _bezierp[i]->updateUIsoParametricLinesForRendering();

                _bezierp[i]->renderUIsoParametricLines(0, GL_LINE_STRIP);
                if (_patch_tangent_visibility)
                {
                    glColor3f(0.0f,1.0f,1.0f);
                    _bezierp[i]->renderUIsoParametricLines(1,GL_LINES);
                }
            }
            if (_show_V_control_lines && i == _selected_bezier_patch)
            {
                _bezierp[i]->updateVIsoParametricLinesForRendering();
                glColor3f(0.0f,1.0f,0.0f);
                _bezierp[i]->renderVIsoParametricLines(0, GL_LINE_STRIP);
                if (_patch_tangent_visibility)
                {
                    glColor3f(0.0f,1.0f,1.0f);
                    _bezierp[i]->renderVIsoParametricLines(1,GL_LINES);
                }
            }
            if (_show_normal_vectors && i == _selected_bezier_patch)
            {
                _bezierp[i]->renderNormalVectors(_normal_scale);
            }
        }

        return true;
    }

    bool GLWidget::_updateSelectedBezierPatch()
    {
        return _bezierp[_selected_bezier_patch]->updateImageForRendering();
    }

    bool GLWidget::_updateAllBezierPatches()
    {
        for (GLuint i=0; i<_bezierp_count; i++)
        {
            _bezierp[_selected_bezier_patch]->updateImageForRendering();
        }
        return true;
    }

    GLdouble GLWidget::_get_bezierp_X(int selected_patch, int selected_patch_pointX, int selected_patch_pointY)
    {
        return (*_bezierp[_selected_bezier_patch])(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY).x();
    }

    GLdouble GLWidget::_get_bezierp_Y(int selected_patch, int selected_patch_pointX, int selected_patch_pointY)
    {
        return (*_bezierp[_selected_bezier_patch])(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY).y();
    }

    GLdouble GLWidget::_get_bezierp_Z(int selected_patch, int selected_patch_pointX, int selected_patch_pointY)
    {
        return (*_bezierp[_selected_bezier_patch])(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY).z();
    }

    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent): QOpenGLWidget(parent)
    {
    }

    GLWidget::~GLWidget()
    {
        _bezier_control_point_obj.DeleteVertexBufferObjects();
        _destroyAllExistingBezierCurvesAndTheirImages();
        _destroyAllExistingBezierPatchesAndTheirImages();
    }

    //--------------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------
    void GLWidget::initializeGL()
    {
        // creating a perspective projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(width()) / static_cast<double>(height());
        _z_near = 1.0;
        _z_far  = 1000.0;
        _fovy   = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

        // enabling the depth test
        glEnable(GL_DEPTH_TEST);

        // setting the background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;

        // ...

        try
        {
            // initializing the OpenGL Extension Wrangler library
            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                "Try to update your driver or buy a new graphics adapter!");
            }

            // create and store your geometry in display lists or vertex buffer objects
            // ...
            if (!_createAllBezierCurvesAndTheirImages())
                throw Exception("Could not create all bezier curves!");

            if (!_createAllBezierPatchesAndTheirImages())
                throw Exception("Could not create all bezier patches!");

            _poz_matrix.ResizeColumns(3);
            for (GLuint i=0; i<3; i++)
            {
                _poz_matrix[i] = new double[16]{0};
                _poz_matrix[i][0] = 1;
                _poz_matrix[i][5] = 1;
                _poz_matrix[i][10] = 1;
                _poz_matrix[i][15] = 1;
                _mx.ResizeRows(3);
                _mx[0] = 0.0;
                _mx[1] = 0.0;
                _mx[2] = 0.0;
            }
            _toonOutlineColor.ResizeColumns(4);
            _toonOutlineColor[0] = 1.0f;
            _toonOutlineColor[1] = 0.0f;
            _toonOutlineColor[2] = 0.0f;
            _toonOutlineColor[3] = 0.0f;
            _shaders.ResizeColumns(4);
            _shaders[0] = new ShaderProgram();
            _shaders[1] = new ShaderProgram();
            _shaders[2] = new ShaderProgram();
            _shaders[3] = new ShaderProgram();
            if (!_shaders[0] || !_shaders[1] || !_shaders[2] || !_shaders[3])
                throw Exception("Could not create all shaders!");
            if (   !_shaders[0]->InstallShaders("Shaders\\directional_light.vert", "Shaders\\directional_light.frag")
                || !_shaders[1]->InstallShaders("Shaders\\reflection_lines.vert", "Shaders\\reflection_lines.frag")
                || !_shaders[2]->InstallShaders("Shaders\\toon.vert", "Shaders\\toon.frag")
                || !_shaders[3]->InstallShaders("Shaders\\two_sided_lighting.vert", "Shaders\\two_sided_lighting.frag"))
            {
                throw Exception("Could not install all shaders!");
            }

            _shaders[1]->Enable();
            _shaders[1]->SetUniformVariable1f("scale_factor", _reflScaleFactor);
            _shaders[1]->SetUniformVariable1f("smoothing", _reflSmoothing);
            _shaders[1]->SetUniformVariable1f("shading", _reflShading);
            _shaders[1]->Disable();

            _shaders[2]->Enable();
            _shaders[2]->SetUniformVariable4f("default_outline_color", _toonOutlineColor[0], _toonOutlineColor[1], _toonOutlineColor[2], _toonOutlineColor[3]);
            _shaders[2]->Disable();
        }
        catch (Exception &e)
        {
            cout << e << endl;
        }
    }

    //-----------------------
    // the rendering function
    //-----------------------
    void GLWidget::paintGL()
    {
        // clears the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // stores/duplicates the original model view matrix
        glPushMatrix();

            // applying transformations
            glTranslated(_mx[0], _mx[1], _mx[2]);
            glRotatef(_angle_x, 1.0, 0.0, 0.0);
            glRotatef(_angle_y, 0.0, 1.0, 0.0);
            glRotatef(_angle_z, 0.0, 0.0, 1.0);
            //glTranslated(_trans_x, _trans_y, _trans_z);
            glScaled(_zoom, _zoom, _zoom);

            // render your geometry (this is oldest OpenGL rendering technique, later we will use some advanced methods)

            if (!_switch_curves_patches)
            {
                _renderAllBezierCurves();
            }
            else
            {
                _renderAllBezierPatches();
            }

        // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
        // i.e., the original model view matrix is restored
        glPopMatrix();
    }

    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(w) / static_cast<double>(h);

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        update();
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            update();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            update();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            update();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            update();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            update();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            update();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            update();
        }
    }

    void GLWidget::setVisibilityOfTangents(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_tangents)
            {
               _show_tangents = false;
               update();
            }
        }
        else
        {
            if (!_show_tangents)
            {
                _show_tangents = true;
                update();
            }
        }
    }

    void GLWidget::setVisibilityOfAccelerationVectors(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_acceleration_vectors)
            {
               _show_acceleration_vectors = false;
               update();
            }
        }
        else
        {
            if (!_show_acceleration_vectors)
            {
                _show_acceleration_vectors = true;
                update();
            }
        }
    }

    void GLWidget::setVisibilityOfControlPoints(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_control_points)
            {
               _show_control_points = false;
               update();
            }
        }
        else
        {
            if (!_show_control_points)
            {
                _show_control_points = true;
                update();
            }
        }
    }

   void GLWidget::set_selected_Obj(int value)
    {
        if (_selected_Obj == value)
            return;
        _selected_Obj = value;
    }

    void GLWidget::set_selected_Obj_Update(int value)
    {
        if (_selected_Obj == value)
            return;
        _selected_Obj = value;
        update();
    }

    void GLWidget::setVisibilityOfCurves(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_curves)
            {
               _show_curves = false;
               update();
            }
        }
        else
        {
            if (!_show_curves)
            {
                _show_curves = true;
                update();
            }
        }
    }

    GLdouble  GLWidget::_get_knots(int selected)
    {
        return _knot_factor[selected];
    }

    void GLWidget::set_bezier_curve_point_X(double value)
    {
        if (_bezierc[_selected_bezier_curve]->getPointCoordinate(_selected_bezier_curve_point).x() == value)
            return;
        _bezierc[_selected_bezier_curve]->setDataPointX(_selected_bezier_curve_point, value);
        _bezierc[_selected_bezier_curve]->updateAllInvolvedForRendering(_selected_bezier_curve_point);
        update();
    }

    void GLWidget::set_bezier_curve_point_Y(double value)
    {
        if (_bezierc[_selected_bezier_curve]->getPointCoordinate(_selected_bezier_curve_point).y() == value)
            return;
        _bezierc[_selected_bezier_curve]->setDataPointY(_selected_bezier_curve_point, value);
        _bezierc[_selected_bezier_curve]->updateAllInvolvedForRendering(_selected_bezier_curve_point);
        update();
    }

    void GLWidget::set_bezier_curve_point_Z(double value)
    {
        if (_bezierc[_selected_bezier_curve]->getPointCoordinate(_selected_bezier_curve_point).z() == value)
            return;
        _bezierc[_selected_bezier_curve]->setDataPointZ(_selected_bezier_curve_point, value);
        _bezierc[_selected_bezier_curve]->updateAllInvolvedForRendering(_selected_bezier_curve_point);
        update();
    }

    void GLWidget::set_selected_bezier_curve(int value)
    {
        if (_selected_bezier_curve == value)
            return;
        if (value >= _bezier_count)
            return;
        _selected_bezier_curve = value;
        update();
    }

    void GLWidget::set_selected_bezier_curve_point(int value)
    {
        if (_selected_bezier_curve_point == value)
            return;
        _selected_bezier_curve_point = value;
        update();
    }

    void GLWidget::set_control_point_scale(double value)
    {
        if (_control_scale == value)
            return;
        _control_scale = value;
        update();
    }

    void GLWidget::setVisibilityOfTexture(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_text)
            {
               _show_text = false;
               update();
            }
        }
        else
        {
            if (!_show_text)
            {
                _show_text = true;
                update();
            }
        }
    }

    void GLWidget::set_bezier_patch_point_X(double value)
    {
        DCoordinate3 point = (*_bezierp[_selected_bezier_patch])(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY);
        if (point.x() == value)
            return;
        point.x() = value;
        _bezierp[_selected_bezier_patch]->setDataPoint(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY,point);
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();
        update();
    }

    void GLWidget::set_bezier_patch_point_Y(double value)
    {
        DCoordinate3 point = (*_bezierp[_selected_bezier_patch])(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY);
        if (point.y() == value)
            return;
        point.y() = value;
        _bezierp[_selected_bezier_patch]->setDataPoint(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY,point);
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();
        update();
    }

    void GLWidget::set_bezier_patch_point_Z(double value)
    {
        DCoordinate3 point = (*_bezierp[_selected_bezier_patch])(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY);
        if (point.z() == value)
            return;
        point.z() = value;
        _bezierp[_selected_bezier_patch]->setDataPoint(_selected_bezier_patch_pointX,_selected_bezier_patch_pointY,point);
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();
        update();
    }

    void GLWidget::set_selected_bezier_patch(int value)
    {
        if (_selected_bezier_patch == value)
            return;
        if (value >= _bezierp_count)
            return;
        _selected_bezier_patch = value;
        update();
    }

    void GLWidget::set_selected_bezier_patch_pointX(int value)
    {
        if (_selected_bezier_patch_pointX == value)
            return;
        _selected_bezier_patch_pointX = value;
        update();
    }

    void GLWidget::set_selected_bezier_patch_pointY(int value)
    {
        if (_selected_bezier_patch_pointY == value)
            return;
        _selected_bezier_patch_pointY = value;
        update();
    }

    void GLWidget::set_control_point_patch_scale(double value)
    {
        if (_control_scale_patch == value)
            return;
        _control_scale_patch = value;
        update();
    }

    void GLWidget::setVisibilityOfPatchControlPoints(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_patch_control_points)
            {
               _show_patch_control_points = false;
               update();
            }
        }
        else
        {
            if (!_show_patch_control_points)
            {
                _show_patch_control_points = true;
                update();
            }
        }
    }

    void GLWidget::setVisibilityOfPatchUControlLines(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_U_control_lines)
            {
               _show_U_control_lines = false;
               update();
            }
        }
        else
        {
            if (!_show_U_control_lines)
            {
                _show_U_control_lines = true;
                update();
            }
        }
    }

    void GLWidget::setVisibilityOfPatchVControlLines(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_V_control_lines)
            {
               _show_V_control_lines = false;
               update();
            }
        }
        else
        {
            if (!_show_V_control_lines)
            {
                _show_V_control_lines = true;
                update();
            }
        }
    }

    void GLWidget::setVisibilityOfPatchNormalVectors(int visibility)
    {
        if (visibility == 0)
        {
            if (_show_normal_vectors)
            {
               _show_normal_vectors = false;
               update();
            }
        }
        else
        {
            if (!_show_normal_vectors)
            {
                _show_normal_vectors = true;
                update();
            }
        }
    }

    void GLWidget::set_control_line_count(int value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_u_line_count())
            return;
        _bezierp[_selected_bezier_patch]->set_u_line_count(value);
        _bezierp[_selected_bezier_patch]->set_v_line_count(value);
        update();
    }

    void GLWidget::mousePressEvent(QMouseEvent* event)
    {
        _mouse_pos_start = event->pos();
    }

    void GLWidget::mouseMoveEvent(QMouseEvent* event)
    {
        if (event->buttons() == 1)
        {
            QPoint diff = event->pos() - _mouse_pos_start;
            _mx[0] += (double) diff.x() / 150;
            _mx[1] -= (double) diff.y() / 150;
            _mouse_pos_start = event->pos();
            update();
            return;
        }
        if(event->buttons() == 2)
        {
            QPoint diff = event->pos() - _mouse_pos_start;
            _angle_x += diff.y();
            _angle_y += diff.x();
            _mouse_pos_start = event->pos();
            update();
        }
    }

    void GLWidget::wheelEvent(QWheelEvent* event)
    {
        if (event->buttons() == 2)
        {
            _zoom += (double) event->angleDelta().y() / 2500;
            if (_zoom < 0) {
                _zoom = 0.001;
            }
            update();
            return;
        }

        _mx[2] += (double) event->angleDelta().y() / 150;
        update();
    }

    GLboolean GLWidget::extendCurve(int side)
    {
        CubicBezierCurve* ext = _bezierc[_selected_bezier_curve]->extend(side);
        if (!ext)
        {
            return GL_FALSE;
        }
        _bezier_count++;
        _bezierc.push_back(ext);
        _bezierc.back()->setDivPointCount(_bezier_div_point_count);
        _bezierc.back()->updateImageForRendering();
        update();
        return GL_TRUE;
    }

    GLboolean GLWidget::mergeCurves(int otherID, int side1, int side2)
    {
        if (_selected_bezier_curve == otherID)
            return GL_FALSE;
        GLboolean toRet = _bezierc[_selected_bezier_curve]->merge(_bezierc[otherID], side1, side2);
        _bezierc[_selected_bezier_curve]->updateAllInvolvedForRendering(3 * side1);

        update();
        return toRet;
    }

    void GLWidget::addCurve()
    {
        ColumnMatrix<DCoordinate3> temp(4);
        temp[1] = DCoordinate3(1,0);
        temp[2] = DCoordinate3(2,0);
        temp[3] = DCoordinate3(3,0);
        _bezier_count++;
        _bezierc.push_back(new CubicBezierCurve(temp));
        _bezierc.back()->setDivPointCount(_bezier_div_point_count);
        _bezierc.back()->updateImageForRendering();
        update();
    }

    void GLWidget::deleteCurve()
    {
        delete _bezierc[_selected_bezier_curve];
        _bezier_count--;
        _bezierc.erase(_bezierc.begin() + _selected_bezier_curve);
        update();
    }

    GLboolean GLWidget::extendPatch(int side)
    {
        BicubicBezierPatch* ext = _bezierp[_selected_bezier_patch]->extend(side);
        if (!ext)
        {
            return GL_FALSE;
        }
        _bezierp_count++;
        _bezierp.push_back(ext);
        _bezierp.back()->set_u_div_point_count(_bezierp_div_point_count_u);
        _bezierp.back()->set_v_div_point_count(_bezierp_div_point_count_v);
        _bezierp.back()->set_u_line_count(4);
        _bezierp.back()->set_v_line_count(4);
        _bezierp.back()->updateImageForRendering();
        update();
        return GL_TRUE;
    }

    GLboolean GLWidget::mergePatches(int otherID, int side1, int side2)
    {
        if (_selected_bezier_patch == otherID)
            return GL_FALSE;
        GLboolean toRet = _bezierp[_selected_bezier_patch]->merge(_bezierp[otherID], side1, side2);
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();

        update();
        return toRet;
    }

    void GLWidget::addPatch()
    {
        BicubicBezierPatch* temp = new BicubicBezierPatch();
        for (GLuint i = 0; i < 4; i++)
        {
            for (GLuint j = 0; j < 4; j++)
            {
                temp->SetData(i, j, DCoordinate3(j,i,0));
            }
        }

        _bezierp_count++;
        _bezierp.push_back(temp);
        _bezierp.back()->set_u_div_point_count(_bezierp_div_point_count_u);
        _bezierp.back()->set_v_div_point_count(_bezierp_div_point_count_v);
        _bezierp.back()->updateImageForRendering();
        update();
    }

    void GLWidget::deletePatch()
    {
        delete _bezierp[_selected_bezier_patch];
        _bezierp_count--;
        _bezierp.erase(_bezierp.begin() + _selected_bezier_patch);
        update();
    }

    void GLWidget::switch_curves_patches(bool change)
    {
        if (_switch_curves_patches == change)
            return;
        _switch_curves_patches = change;
        update();
    }

    void GLWidget::translate_patch(GLdouble x,GLdouble y, GLdouble z)
    {
        _bezierp[_selected_bezier_patch]->translatePatch(DCoordinate3(x,y,z));
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();
        update();
    }

    void GLWidget::translate_whole(GLdouble x,GLdouble y,GLdouble z)
    {
        _bezierp[_selected_bezier_patch]->translateWhole(DCoordinate3(x,y,z));
        _bezierp[_selected_bezier_patch]->updateAllInvolvedForRendering();
        update();
    }

    void GLWidget::set_light_pos(GLdouble x,GLdouble y,GLdouble z, GLdouble w)
    {
        if (w == 0.0)
            w = 0.00001;
        _light_position[_selected_light] = HCoordinate3(x,y,z,w);
        update();
    }

    void GLWidget::set_light_ambience(GLdouble r,GLdouble g,GLdouble b, GLdouble a)
    {
        _light_ambient_intensity[_selected_light] = Color4(r,g,b,a);
        update();
    }

    void GLWidget::set_light_diffuse(GLdouble r,GLdouble g,GLdouble b, GLdouble a)
    {
        _light_diffuse_intensity[_selected_light] = Color4(r,g,b,a);
        update();
    }

    void GLWidget::set_light_specular(GLdouble r,GLdouble g,GLdouble b, GLdouble a)
    {
        _light_specular_intensity[_selected_light] = Color4(r,g,b,a);
        update();
    }

    void GLWidget::set_light_constant(GLdouble val)
    {
        _light_constant_attenuation[_selected_light] = val;
        update();
    }

    void GLWidget::set_light_linear(GLdouble val)
    {
        _light_linear_attenuation[_selected_light] = val;
        update();
    }

    void GLWidget::set_light_quadratic(GLdouble val)
    {
        _light_quadratic_attenuation[_selected_light] = val;
        update();
    }

    void GLWidget::set_light_spot_dir(GLdouble x,GLdouble y,GLdouble z)
    {
        _light_spot_direction[_selected_light] = HCoordinate3(x,y,z);
        update();
    }

    void GLWidget::set_light_spot_cutoff(GLdouble val)
    {
        val = clamp(val, 0., 90.);
        _light_spot_cutoff[_selected_light] = val;
        update();
    }

    void GLWidget::set_light_spot_exponent(GLdouble val)
    {
        val = clamp(val, 1., 128.);
        _light_spot_exponent[_selected_light] = val;
        update();
    }

    void GLWidget::set_light_type(int id)
    {
        if (id == _selected_light)
            return;
        _selected_light = id;
        update();
    }

    void GLWidget::set_selected_bezier_patch_color_R(int value)
    {
        if ((double)value / 255 == _bezierp[_selected_bezier_patch]->getColorR())
            return;
        _bezierp[_selected_bezier_patch]->setColorR((double)value / 255);
        update();
    }
    void GLWidget::set_selected_bezier_patch_color_G(int value)
    {
        if ((double)value / 255 == _bezierp[_selected_bezier_patch]->getColorG())
            return;
        _bezierp[_selected_bezier_patch]->setColorG((double)value / 255);
        update();
    }
    void GLWidget::set_selected_bezier_patch_color_B(int value)
    {
        if ((double)value / 255 == _bezierp[_selected_bezier_patch]->getColorB())
            return;
        _bezierp[_selected_bezier_patch]->setColorB((double)value / 255);
        update();
    }

    int GLWidget::get_selected_bezier_patch_color_R()
    {
        return floor(_bezierp[_selected_bezier_patch]->getColorR() * 255);
    }

    int GLWidget::get_selected_bezier_patch_color_G()
    {
        return floor(_bezierp[_selected_bezier_patch]->getColorG() * 255);
    }
    int GLWidget::get_selected_bezier_patch_color_B()
    {
        return floor(_bezierp[_selected_bezier_patch]->getColorB() * 255);
    }

    GLboolean GLWidget::jointPatches(int otherID, int side1, int side2)
    {
        if (_selected_bezier_patch == otherID)
            return GL_FALSE;

        BicubicBezierPatch* ext = _bezierp[_selected_bezier_patch]->join(_bezierp[otherID], side1, side2);
        if (!ext)
        {
            return GL_FALSE;
        }
        _bezierp_count++;
        _bezierp.push_back(ext);
        _bezierp.back()->set_u_div_point_count(_bezierp_div_point_count_u);
        _bezierp.back()->set_v_div_point_count(_bezierp_div_point_count_v);
        _bezierp.back()->set_u_line_count(4);
        _bezierp.back()->set_v_line_count(4);
        _bezierp.back()->updateImageForRendering();
        update();
        return GL_TRUE;
    }

    GLboolean GLWidget::joinCurves(int otherID, int side1, int side2)
    {
        if (_selected_bezier_curve == otherID)
            return GL_FALSE;

        CubicBezierCurve* ext = _bezierc[_selected_bezier_curve]->join(_bezierc[otherID], side1, side2);
        if (!ext)
        {
            return GL_FALSE;
        }
        _bezier_count++;
        _bezierc.push_back(ext);
        _bezierc.back()->setDivPointCount(_bezier_div_point_count);
        _bezierc.back()->updateImageForRendering();
        update();
        return GL_TRUE;
    }

    void GLWidget::set_selected_shader(int value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_selected_shader())
            return;
        _bezierp[_selected_bezier_patch]->set_selected_shader(value);
        update();
    }

    void GLWidget::set_refl_scale(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_refl_scale())
            return;
        _bezierp[_selected_bezier_patch]->set_refl_scale(value);
        update();
    }

    void GLWidget::set_refl_smoothing(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_refl_smoothing())
            return;
        _bezierp[_selected_bezier_patch]->set_refl_smoothing(value);
        update();
    }

    void GLWidget::set_refl_shading(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_refl_shading())
            return;
        _bezierp[_selected_bezier_patch]->set_refl_shading(value);
        update();
    }

    void GLWidget::set_toon_color_R(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_toon_color_R())
            return;
        _bezierp[_selected_bezier_patch]->set_toon_color_R(value);
        update();
    }

    void GLWidget::set_toon_color_G(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_toon_color_G())
            return;
        _bezierp[_selected_bezier_patch]->set_toon_color_G(value);
        update();
    }

    void GLWidget::set_toon_color_B(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_toon_color_B())
            return;
        _bezierp[_selected_bezier_patch]->set_toon_color_B(value);
        update();
    }

    void GLWidget::set_toon_color_A(double value)
    {
        if (value == _bezierp[_selected_bezier_patch]->get_toon_color_A())
            return;
        _bezierp[_selected_bezier_patch]->set_toon_color_A(value);
        update();
    }

    void GLWidget::set_selected_bezier_patch_material(int value)
    {
        if (value == _bezierp[_selected_bezier_patch]->getMaterial())
            return;
        _bezierp[_selected_bezier_patch]->setMaterial(value);
        update();
    }

    void GLWidget::setUseOfPatchColors(bool visibility)
    {
        if (!visibility)
        {
            if (_use_colors)
            {
               _use_colors = false;
               update();
            }
        }
        else
        {
            if (!_use_colors)
            {
                _use_colors = true;
                update();
            }
        }
    }

    void GLWidget::setUseOfPatchMaterials(bool visibility)
    {
        if (!visibility)
        {
            if (_use_materials)
            {
               _use_materials = false;
               update();
            }
        }
        else
        {
            if (!_use_materials)
            {
                _use_materials = true;
                update();
            }
        }
    }
    void GLWidget::setUseOfPatchShaders(bool visibility)
    {
        if (!visibility)
        {
            if (_use_shaders)
            {
               _use_shaders = false;
               update();
            }
        }
        else
        {
            if (!_use_shaders)
            {
                _use_shaders = true;
                update();
            }
        }
    }

    void GLWidget::setVisibilityOfPatchTangents(int visibility)
    {
        if (visibility == 0)
        {
            if (_patch_tangent_visibility)
            {
               _patch_tangent_visibility = false;
               update();
            }
        }
        else
        {
            if (!_patch_tangent_visibility)
            {
                _patch_tangent_visibility = true;
                update();
            }
        }
    }

    void GLWidget::set_selected_bezier_curve_color_R(int value)
    {
        if ((double)value / 255 == _bezierc[_selected_bezier_curve]->getColorR())
            return;
        _bezierc[_selected_bezier_curve]->setColorR((double)value / 255);
        update();
    }

    void GLWidget::set_selected_bezier_curve_color_G(int value)
    {
        if ((double)value / 255 == _bezierc[_selected_bezier_curve]->getColorG())
            return;
        _bezierc[_selected_bezier_curve]->setColorG((double)value / 255);
        update();
    }

    void GLWidget::set_selected_bezier_curve_color_B(int value)
    {
        if ((double)value / 255 == _bezierc[_selected_bezier_curve]->getColorB())
            return;
        _bezierc[_selected_bezier_curve]->setColorB((double)value / 255);
        update();
    }

    int GLWidget::get_selected_bezier_curve_color_R()
    {
        return floor(_bezierc[_selected_bezier_curve]->getColorR() * 255);
    }
    int GLWidget::get_selected_bezier_curve_color_G()
    {
        return floor(_bezierc[_selected_bezier_curve]->getColorG() * 255);
    }
    int GLWidget::get_selected_bezier_curve_color_B()
    {
        return floor(_bezierc[_selected_bezier_curve]->getColorB() * 255);
    }

    void GLWidget::saveCurves()
    {
        ofstream file("curveSave.off");
        file << _bezier_count << std::endl;
        for (GLuint i = 0; i < _bezier_count; i++)
            file << (*_bezierc[i]);
        int conns[2];
        for (GLuint i = 0; i < _bezier_count; i++)
        {
            _bezierc[i]->getConn(_bezierc, conns);
            for (int j = 0; j < 2; j++)
                file << conns[j] << " ";
            file << std::endl;
        }
    }

    void GLWidget::loadCurves()
    {
        _destroyAllExistingBezierCurvesAndTheirImages();
        _bezierc.clear();
        ifstream file("curveSave.in");
        if (file.bad())
            return;
        CubicBezierCurve* temp;
        ColumnMatrix<DCoordinate3> t;
        _selected_bezier_curve = 0;

        file >> _bezier_count;
        for (GLuint i = 0; i < _bezier_count; i++)
        {
            temp = new CubicBezierCurve(t);
            file >> (*temp);
            _bezierc.push_back(temp);
        }

        GLint conns[2];
        for (GLuint i = 0; i < _bezier_count; i++)
        {
            for (int j = 0; j < 2; j++)
                file >> conns[j];
            _bezierc[i]->setConn(_bezierc, conns);
            _bezierc[i]->updateImageForRendering();
        }
        update();
    }

    void GLWidget::savePatches()
    {
        ofstream file("patchSave.off");
        file << _bezierp_count << std::endl;
        for (GLuint i = 0; i < _bezierp_count; i++)
            file << (*_bezierp[i]);
        GLint conns[8];
        for (GLuint i = 0; i < _bezierp_count; i++)
        {
            _bezierp[i]->getConn(_bezierp, conns);
            for (int j = 0; j < 8; j++)
                file << conns[j] << " ";
            file << std::endl;
        }
    }

    void GLWidget::loadPatches()
    {
        _destroyAllExistingBezierPatchesAndTheirImages();
        _bezierp.clear();
        ifstream file("patchSave.in");
        if (file.bad())
            return;
        BicubicBezierPatch* temp;
        _selected_bezier_patch = 0;

        file >> _bezierp_count;
        cout <<"size " << _bezierp_count << endl;
        for (GLuint i = 0; i < _bezierp_count; i++)
        {
            temp = new BicubicBezierPatch();
            file >> (*temp);
            cout << (*temp);
            _bezierp.push_back(temp);
        }

        int conns[8];
        for (GLuint i = 0; i < _bezierp_count; i++)
        {
            for (int j = 0; j < 8; j++)
                file >> conns[j];
            _bezierp[i]->setConn(_bezierp, conns);
            _bezierp[i]->updateImageForRendering();
            if (_show_U_control_lines)
                _bezierp[i]->updateUIsoParametricLinesForRendering();
            if (_show_V_control_lines)
                _bezierp[i]->updateVIsoParametricLinesForRendering();
        }
        update();
    }

    int GLWidget::get_bezierc_count()
    {
        return _bezier_count;
    }

    int GLWidget::get_bezierp_count()
    {
        return _bezierp_count;
    }

    int GLWidget::get_patch_shader()
    {
        return _bezierp[_selected_bezier_patch]->get_selected_shader();
    }
}
