#pragma once

#include <QMainWindow>
#include "ui_MainWindow.h"
#include "SideWidget.h"
#include "GLWidget.h"
#include <QScrollArea>

namespace cagd
{
    class MainWindow: public QMainWindow, public Ui::MainWindow
    {
        Q_OBJECT

    private:
        SideWidget  *_side_widget;      // pointer to our side widget
        GLWidget    *_gl_widget;        // pointer to our rendering OpenGL widget

        QScrollArea *_scroll_area;      // provides a scrolling view onto our side widget

    public:
        // special and default constructor
        MainWindow(QWidget *parent = 0);

    private slots:

        // private event handling methods/slots
        void on_action_Quit_triggered();

        void update_bezier_curve_XYZ(int);
        void update_bezier_curve_point_XYZ(int);

        void update_bezier_patch_XYZ(int);
        void update_bezier_patch_pointX_XYZ(int);
        void update_bezier_patch_pointY_XYZ(int);
        void update_bezier_patch_line_count(int);

        void check_curve(bool);
        void check_patch(bool);

        void save_curves();
        void load_curves();

        void save_patches();
        void load_patches();

        void merge_curves();
        void join_curves();
        void extend_curve();
        void add_curves();
        void delete_curves();

        void merge_patches();
        void join_patches();
        void extend_patch();
        void add_patch();
        void delete_patch();

        void translate_patch();
        void translate_whole();

        void update_bezier_patch_RGB(int);
        void update_bezier_curve_RGB(int);

        void set_light_type(int);
        void set_light_pos();
        void set_light_ambience();
        void set_light_diffuse();
        void set_light_specular();
        void set_light_constant();
        void set_light_linear();
        void set_light_quadratic();
        void set_light_spot_dir();
        void set_light_spot_cutoff();
        void set_light_spot_exponent();

        void set_shader_param1(double);
        void set_shader_param2(double);
        void set_shader_param3(double);
        void set_shader_param4(double);

        void set_shader_mods(int);
    };
}
