#include "SideWidget.h"

namespace cagd
{
    SideWidget::SideWidget(QWidget *parent): QWidget(parent)
    {
        setupUi(this);
        bezier_curve_ID->addItem("1");
        bezier_curve_ID->addItem("2");
        bezier_curve_ID->addItem("3");
        bezier_curve_ID->addItem("4");
        bezier_curve_ID->addItem("5");
        bezier_curve_ID->addItem("6");
        bezier_curve_ID->addItem("7");
        bezier_curve_ID->addItem("8");
        bezier_curve_ID->addItem("9");

        bezier_curve_other_ID->addItem("1");
        bezier_curve_other_ID->addItem("2");
        bezier_curve_other_ID->addItem("3");
        bezier_curve_other_ID->addItem("4");
        bezier_curve_other_ID->addItem("5");
        bezier_curve_other_ID->addItem("6");
        bezier_curve_other_ID->addItem("7");
        bezier_curve_other_ID->addItem("8");
        bezier_curve_other_ID->addItem("9");

        bezier_curve_side->addItem("Red");
        bezier_curve_side->addItem("Blue");

        bezier_curve_other_side->addItem("Red");
        bezier_curve_other_side->addItem("Blue");


        bezier_patch_ID->addItem("1");
        bezier_patch_ID->addItem("2");
        bezier_patch_ID->addItem("3");
        bezier_patch_ID->addItem("4");
        bezier_patch_ID->addItem("5");
        bezier_patch_ID->addItem("6");
        bezier_patch_ID->addItem("7");
        bezier_patch_ID->addItem("8");
        bezier_patch_ID->addItem("9");

        bezier_patch_other_ID->addItem("1");
        bezier_patch_other_ID->addItem("2");
        bezier_patch_other_ID->addItem("3");
        bezier_patch_other_ID->addItem("4");
        bezier_patch_other_ID->addItem("5");
        bezier_patch_other_ID->addItem("6");
        bezier_patch_other_ID->addItem("7");
        bezier_patch_other_ID->addItem("8");
        bezier_patch_other_ID->addItem("9");

        bezier_patch_side->addItem("Left / Blue");
        bezier_patch_side->addItem("Top / Purple");
        bezier_patch_side->addItem("Right / Cyan");
        bezier_patch_side->addItem("Bottom / Yellow");

        bezier_patch_other_side->addItem("Left / Blue");
        bezier_patch_other_side->addItem("Top / Purple");
        bezier_patch_other_side->addItem("Right / Cyan");
        bezier_patch_other_side->addItem("Bottom / Yellow");


        bezier_curve_point_ID->addItem("1");
        bezier_curve_point_ID->addItem("2");
        bezier_curve_point_ID->addItem("3");
        bezier_curve_point_ID->addItem("4");

        bezier_patch_point_ID_X->addItem("1");
        bezier_patch_point_ID_X->addItem("2");
        bezier_patch_point_ID_X->addItem("3");
        bezier_patch_point_ID_X->addItem("4");

        bezier_patch_point_ID_Y->addItem("1");
        bezier_patch_point_ID_Y->addItem("2");
        bezier_patch_point_ID_Y->addItem("3");
        bezier_patch_point_ID_Y->addItem("4");

        bezier_patch_material->addItem("Brass");
        bezier_patch_material->addItem("Gold");
        bezier_patch_material->addItem("Silver");
        bezier_patch_material->addItem("Emerald");
        bezier_patch_material->addItem("Pearl");
        bezier_patch_material->addItem("Ruby");
        bezier_patch_material->addItem("Turquoise");

        bezier_patch_shader_type->addItem("Directional Light");
        bezier_patch_shader_type->addItem("Reflection Lines");
        bezier_patch_shader_type->addItem("Toon");
        bezier_patch_shader_type->addItem("Two Sided Lighting");

        bezier_patch_Light_Type_ID->addItem("Directional Light");
        bezier_patch_Light_Type_ID->addItem("Point Light");
        bezier_patch_Light_Type_ID->addItem("Specular Light");

        bezier_patch_shader_A->setDisabled(true);
        bezier_patch_shader_R->setDisabled(true);
        bezier_patch_shader_G->setDisabled(true);
        bezier_patch_shader_B->setDisabled(true);
        bezier_patch_shader_A_label->setHidden(true);
        bezier_patch_shader_R_label->setHidden(true);
        bezier_patch_shader_G_label->setHidden(true);
        bezier_patch_shader_B_label->setHidden(true);

        curve_Error->setHidden(true);
        patch_Error->setHidden(true);

        lightConstantButton->setEnabled(false);
        lightLinearButton->setEnabled(false);
        lightQuadraticButton->setEnabled(false);
        lightSpotDirButton->setEnabled(false);
        lightCutoffButton->setEnabled(false);
        lightExponantButton->setEnabled(false);
    }
}
