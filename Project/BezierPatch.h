#pragma once
#include "../Core/TensorProductSurfaces3.h"
#include "../Core/Materials.h"
namespace cagd
{
    class BicubicBezierPatch: public TensorProductSurface3
    {
        BicubicBezierPatch*     _attached[8] = {nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr};
        GLuint                  _attached_side[8] = {0,0,0,0,0,0,0,0};
        // Left, Up, Right, Down

        GLuint             _u_div_point_count;
        GLuint             _v_div_point_count;
        TriangulatedMesh3* _image = nullptr;

        GLuint             _u_line_count;
        GLuint             _v_line_count;
        RowMatrix<GenericCurve3*>* _UIsoParametricLines = nullptr;
        RowMatrix<GenericCurve3*>* _VIsoParametricLines = nullptr;
        bool               _modified[4] = {false, false, false, false};
        GLdouble           _color[3]{0};
        GLuint             _mat = 0;

        GLuint              _selected_shader = 0;
        GLfloat             _reflScaleFactor = 1.0f;
        GLfloat             _reflSmoothing = 1.0f;
        GLfloat             _reflShading = 1.0f;
        GLint               _currShader = 0;
        RowMatrix<GLfloat>  _toonOutlineColor;

        void setNorthWest(GLuint, GLuint, DCoordinate3);
        void setNorthEast(GLuint, GLuint, DCoordinate3);
        void setSouthWest(GLuint, GLuint, DCoordinate3);
        void setSouthEast(GLuint, GLuint, DCoordinate3);
        void setDataPoints(GLuint, GLuint, DCoordinate3);
        void setDataPointInner(GLuint, GLuint, DCoordinate3);
        void unSet();
        void unSetRender();
        void Set();
        void innertranslateWhole(DCoordinate3);

        void rotX(GLint, GLuint[4][4]);
        void rotY(GLint, GLuint[4][4]);

        friend std::istream& operator >> (std::istream&, BicubicBezierPatch& rhs);
        friend std::ostream& operator << (std::ostream&, const BicubicBezierPatch& rhs);

    public:

        BicubicBezierPatch();
        GLboolean UBlendingFunctionValues(GLdouble uknot, RowMatrix<GLdouble>& blendingvalues) const;
        GLboolean VBlendingFunctionValues(GLdouble vknot, RowMatrix<GLdouble>& blendingvalues) const;
        GLboolean CalculatePartialDerivatives(GLuint maximumorderofpartialderivatives,
                                             GLdouble u, GLdouble v, PartialDerivatives& pd) const;

        GLboolean setDataPoint(GLuint, GLuint, DCoordinate3);
        GLboolean setDataPointX(GLuint, GLuint, GLdouble);
        GLboolean setDataPointY(GLuint, GLuint, GLdouble);
        GLboolean setDataPointZ(GLuint, GLuint, GLdouble);


        void set_selected_shader(int value);
        void set_refl_scale(double value);
        void set_refl_smoothing(double value);
        void set_refl_shading(double value);

        void set_toon_color_R(double value);
        void set_toon_color_G(double value);
        void set_toon_color_B(double value);
        void set_toon_color_A(double value);

        int get_selected_shader();
        double get_refl_scale();
        double get_refl_smoothing();
        double get_refl_shading();

        double get_toon_color_R();
        double get_toon_color_G();
        double get_toon_color_B();
        double get_toon_color_A();

        BicubicBezierPatch* extend(GLuint);
        BicubicBezierPatch* join(BicubicBezierPatch* other, GLuint thisSide, GLuint otherSide);
        BicubicBezierPatch* join2(BicubicBezierPatch* other, GLuint thisSide, GLuint otherSide);
        GLboolean merge(BicubicBezierPatch*, GLuint, GLuint);

        void translatePatch(DCoordinate3);
        void translateWhole(DCoordinate3);

        void attach(GLuint,GLuint,BicubicBezierPatch*);
        void detach(GLuint);

        GLboolean updateImageForRendering();
        GLboolean updateAllInvolvedForRendering();
        GLboolean updateUIsoParametricLinesForRendering();
        GLboolean updateVIsoParametricLinesForRendering();

        GLboolean render();
        GLboolean renderNormalVectors(GLdouble = 1.0);
        GLboolean renderUIsoParametricLines(GLuint, GLenum);
        GLboolean renderVIsoParametricLines(GLuint, GLenum);

        void getConn(std::vector<BicubicBezierPatch*>&, GLint[8]);
        void setConn(std::vector<BicubicBezierPatch*>&, GLint[8]);

        void set_u_div_point_count(GLuint);
        void set_v_div_point_count(GLuint);
        void set_u_line_count(GLuint);
        void set_v_line_count(GLuint);

        GLuint get_u_div_point_count() const;
        GLuint get_v_div_point_count() const;
        GLuint get_u_line_count() const;
        GLuint get_v_line_count() const;

        void setColorR(GLdouble);
        void setColorG(GLdouble);
        void setColorB(GLdouble);

        GLdouble getColorR() const;
        GLdouble getColorG() const;
        GLdouble getColorB() const;

        void    setMaterial(GLuint);
        GLuint  getMaterial() const;

        ~BicubicBezierPatch();
    };

    inline std::istream& operator >> (std::istream& in, BicubicBezierPatch& rhs)
    {
        in >> rhs._data;
        for(GLuint i = 0; i < 8; i++)
        {
            in >> rhs._attached_side[i];
        }

        in >> rhs._u_div_point_count;
        in >> rhs._v_div_point_count;

        in >> rhs._color[0] >> rhs._color[1] >> rhs._color[2];
        in >> rhs._mat;
        in >> rhs._selected_shader;
        in >> rhs._reflScaleFactor;
        in >> rhs._reflSmoothing;
        in >> rhs._reflShading;
        in >> rhs._currShader;
        in >> rhs._toonOutlineColor;
        return in;
    }

    inline std::ostream& operator << (std::ostream& out, const BicubicBezierPatch& rhs)
    {
        out << rhs._data << std::endl;
        for(GLuint i = 0; i < 8; i++)
        {
            out << rhs._attached_side[i] << ' ';
        }
        out << std::endl;

        out << rhs._u_div_point_count << " ";
        out << rhs._v_div_point_count;
        out << std::endl;

        out << rhs._color[0] << " " << rhs._color[1] << " " << rhs._color[2];
        out << std::endl;
        out << rhs._mat;
        out << std::endl;
        out << rhs._selected_shader;
        out << std::endl;
        out << rhs._reflScaleFactor;
        out << std::endl;
        out << rhs._reflSmoothing;
        out << std::endl;
        out << rhs._reflShading;
        out << std::endl;
        out << rhs._currShader;
        out << std::endl;
        out << rhs._toonOutlineColor;
        out << std::endl;
        return out;
    }
}
