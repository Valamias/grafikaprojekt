#pragma once

#include "../Core/LinearCombination3.h"
#include "../Core/Matrices.h"
#include "../Core/DCoordinates3.h"

namespace cagd
{

    class CubicBezierCurve: public LinearCombination3
    {
        CubicBezierCurve*   _attached[2] = {nullptr, nullptr};
        GLboolean           _attached_side[2] = {0, 0};

        GLuint              _div_point_count;
        GenericCurve3*      _image = nullptr;
        GLdouble            _color[3] = {1., 0., 0.};

        friend std::istream& operator >> (std::istream&, CubicBezierCurve& rhs);
        friend std::ostream& operator << (std::ostream&, const CubicBezierCurve& rhs);

    public:

        GLboolean setDataPoint(GLuint i, DCoordinate3 point);
        GLboolean setDataPointX(GLuint i, GLdouble x);
        GLboolean setDataPointY(GLuint i, GLdouble y);
        GLboolean setDataPointZ(GLuint i, GLdouble z);

        GLboolean updateImageForRendering();
        GLboolean updateAllInvolvedForRendering(GLuint);

        void setDivPointCount(GLuint);
        GLuint getDivPointCount() const;

        DCoordinate3 getPointCoordinate(GLuint) const;

        GLboolean render(GLuint,GLenum);

        CubicBezierCurve(ColumnMatrix<DCoordinate3> &data, GLenum data_usage_flag = GL_STATIC_DRAW);

        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;

        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const;

        CubicBezierCurve* extend(GLboolean side);

        CubicBezierCurve* join(CubicBezierCurve*, GLboolean, GLboolean);

        GLboolean merge(CubicBezierCurve*, GLboolean, GLboolean);

        void UpdateData(ColumnMatrix<DCoordinate3> &data);

        GLboolean attach(GLboolean, GLboolean, CubicBezierCurve*);

        void detach(GLuint);

        GLdouble getColorR() const;
        GLdouble getColorG() const;
        GLdouble getColorB() const;

        void setColorR(GLdouble);
        void setColorG(GLdouble);
        void setColorB(GLdouble);

        void getConn(std::vector<CubicBezierCurve*>& ind, GLint ret[2]);
        void setConn(std::vector<CubicBezierCurve*>& ind, GLint ret[2]);

        ~CubicBezierCurve();
    };

    inline std::istream& operator >> (std::istream& in, CubicBezierCurve& rhs)
    {
        in >> rhs._data;
        int temp1, temp2;
        in >> temp1 >> temp2;
        rhs._attached_side[0] = (temp1 == 1);
        rhs._attached_side[1] = (temp2 == 1);
        in >> rhs._div_point_count;
        in >> rhs._color[0] >> rhs._color[1] >> rhs._color[2];
        return in;
    }

    inline std::ostream& operator << (std::ostream& out, const CubicBezierCurve& rhs)
    {
        out << rhs._data << std::endl;
        out << (int)rhs._attached_side[0] << " " << (int)rhs._attached_side[1] << std::endl;
        out << rhs._div_point_count << std::endl;
        out << rhs._color[0] << " " << rhs._color[1] << " " << rhs._color[2] << std::endl;
        return out;
    }
}
