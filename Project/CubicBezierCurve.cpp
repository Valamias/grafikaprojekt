#include "CubicBezierCurve.h"

namespace cagd
{
    CubicBezierCurve::CubicBezierCurve(ColumnMatrix<DCoordinate3> &data, GLenum data_usage_flag): LinearCombination3(0.0, 1.0, 4, data_usage_flag)
    {
        if (data.GetRowCount() == 4)
        {
            _data = data;
        }
        else
        {
            _data.ResizeRows(4);
        }
    }

    GLboolean CubicBezierCurve::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const
    {
        if (u < 0.0 || u > 1.0)
            return GL_FALSE;

        values.ResizeColumns(4);
        GLdouble w = 1.0 - u;

        values[0] = w * w * w;
        values[1] = 3 * w * w * u;
        values[2] = 3 * w * u * u;
        values[3] = u * u * u;

        return GL_TRUE;
    }

    GLboolean CubicBezierCurve::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const
    {
        if (u < 0.0 || u > 1.0 || max_order_of_derivatives > 2)
            return GL_FALSE;

        ColumnMatrix<RowMatrix<GLdouble>> u_blending_values(3);

        GLdouble t = 1 - u;

        u_blending_values[0].ResizeColumns(4);
        u_blending_values[0][3] = u * u * u;
        u_blending_values[0][2] = 3 * t * u * u;
        u_blending_values[0][1] = 3 * t * t * u;
        u_blending_values[0][0] = t * t * t;

        if (max_order_of_derivatives > 0)
        {
            u_blending_values[1].ResizeColumns(4);
            u_blending_values[1][3] = 3 * u * u;
            u_blending_values[1][2] = 6 * t * u - u * u * 3;
            u_blending_values[1][1] = 3 * t * t  - 6 * t * u;
            u_blending_values[1][0] = -3 * t * t;

            if (max_order_of_derivatives > 1)
            {
                u_blending_values[2].ResizeColumns(4);
                u_blending_values[2][3] = 6 * u;
                u_blending_values[2][2] = 6 * (t - u * 2);
                u_blending_values[2][1] = -6 * (2 * t - u);
                u_blending_values[2][0] = 6 * t;
            }
        }

        d.ResizeRows(max_order_of_derivatives + 1);
        d.LoadNullVectors();

        for (GLuint i = 0; i <= max_order_of_derivatives; i++)
        {
            for (GLuint j = 0; j < 4; j++)
            {
                d[i] += u_blending_values[i][j] * _data[j];
            }
        }

        return GL_TRUE;
    }

    GLboolean CubicBezierCurve::setDataPoint(GLuint i, DCoordinate3 point)
    {
        if (i > 3)
            return GL_FALSE;
        if (point == _data[i])
            return GL_TRUE;
        CubicBezierCurve* workSide = _attached[i>1];
        DCoordinate3 offset = point - _data[i];
        _data[i] = point;
        if (workSide)
        {
            if (i == 0 || i == 3)
            {
                _data[1 + (i>1)] += offset;
                workSide->_data[_attached_side[i>1]*3] = point;
                workSide->_data[1 + _attached_side[i>1]] += offset;
            }
            else
            {
                workSide->_data[1 + _attached_side[i>1]] -= offset;
            }
        }

        return GL_TRUE;
    }

    void CubicBezierCurve::detach(GLuint i)
    {
        if (i < 2)
            _attached[i] = nullptr;
    }

    CubicBezierCurve::~CubicBezierCurve()
    {
        for (GLuint i=0; i < 2; i++)
        {
            if (_attached[i])
            {
                _attached[i]->detach(_attached_side[i]);
                _attached[i] = nullptr;
            }
        }

        DeleteVertexBufferObjectsOfData();
        if (_image)
        {
            _image->DeleteVertexBufferObjects();
            delete _image;
            _image = nullptr;
        }
    }

    GLboolean CubicBezierCurve::setDataPointX(GLuint i, GLdouble x)
    {
        DCoordinate3 newX = _data[i];
        newX.x() = x;
        return setDataPoint(i, newX);
    }

    GLboolean CubicBezierCurve::setDataPointY(GLuint i, GLdouble y)
    {
        DCoordinate3 newY = _data[i];
        newY.y() = y;
        return setDataPoint(i, newY);
    }

    GLboolean CubicBezierCurve::setDataPointZ(GLuint i, GLdouble z)
    {
        DCoordinate3 newZ = _data[i];
        newZ.z() = z;
        return setDataPoint(i, newZ);
    }

    GLboolean CubicBezierCurve::attach(GLboolean side1, GLboolean side2, CubicBezierCurve* curve)
    {
        if (_attached[side1])
            return GL_FALSE;
        _attached[side1] = curve;
        _attached_side[side1] = side2;
        return GL_TRUE;
    }

    CubicBezierCurve* CubicBezierCurve::extend(GLboolean side)
    {
        if (_attached[side])
            return nullptr;
        CubicBezierCurve* extension;
        ColumnMatrix<DCoordinate3> data(4);
        GLint curr = (!side)*3;
        GLint next = 1 - 2*(!side);
        data[curr] = _data[side*3];
        data[curr + next] = 2 * _data[side*3] - _data[1 + side];
        curr += next;
        data[curr + next] = 2 * data[curr] - data[curr - next];
        curr += next;
        data[curr + next] = 2 * data[curr] - data[curr - next];

        extension = new CubicBezierCurve(data);
        if (!extension)
            return nullptr;
        _attached[side] = extension;
        _attached_side[side] = !side;

        extension->_attached[!side] = this;
        extension->_attached_side[!side] = side;

        return extension;
    }

    GLboolean CubicBezierCurve::merge(CubicBezierCurve* other, GLboolean thisSide, GLboolean otherSide)
    {
        if (_attached[thisSide] || other->_attached[otherSide])
            return GL_FALSE;
        _data[thisSide*3] = 1.0/2 * (_data[1 + thisSide] + other->_data[1 + otherSide]);
        other->_data[otherSide*3] = _data[thisSide*3];

        _attached[thisSide] = other;
        _attached_side[thisSide] = otherSide;

        other->_attached[otherSide] = this;
        other->_attached_side[otherSide] = thisSide;

        return GL_TRUE;
    }

    GLboolean CubicBezierCurve::updateImageForRendering()
    {
        DeleteVertexBufferObjectsOfData();
        if (!UpdateVertexBufferObjectsOfData())
        {
            std::cout << "Couln't update curve!" << std::endl;
            return GL_FALSE;
        }
        if (_image)
        {
            _image -> DeleteVertexBufferObjects();
            delete _image;
        }
        _image = GenerateImage(2,_div_point_count);

        if (!_image)
        {
            std::cout << "Couln't create curve image!" << std::endl;
            return GL_FALSE;
        }

        if (!_image->UpdateVertexBufferObjects())
        {
            std::cout << "Couln't update curve vertexbuffer objects!" << std::endl;
            return GL_FALSE;
        }

        return GL_TRUE;
    }

    GLboolean CubicBezierCurve::render(GLuint der, GLenum render_mode)
    {
        if (!_image)
            return GL_FALSE;

        return _image->RenderDerivatives(der, render_mode);
    }

    GLboolean CubicBezierCurve::updateAllInvolvedForRendering(GLuint point)
    {
        updateImageForRendering();
        if (point < 2)
        {
            if (_attached[0])
                _attached[0]->updateImageForRendering();
        }
        else
        {
            if (_attached[1])
                _attached[1]->updateImageForRendering();
        }
        return GL_TRUE;
    }


    void CubicBezierCurve::setDivPointCount(GLuint count)
    {
        _div_point_count = count;
    }

    GLuint CubicBezierCurve::getDivPointCount() const
    {
        return _div_point_count;
    }

    DCoordinate3 CubicBezierCurve::getPointCoordinate(GLuint point) const
    {
        return _data[point];
    }

    GLdouble CubicBezierCurve::getColorR() const
    {
        return _color[0];
    }
    GLdouble CubicBezierCurve::getColorG() const
    {
        return _color[1];
    }
    GLdouble CubicBezierCurve::getColorB() const
    {
        return _color[2];
    }

    void CubicBezierCurve::setColorR(GLdouble r)
    {
        _color[0] = r;
    }
    void CubicBezierCurve::setColorG(GLdouble g)
    {
        _color[1] = g;
    }
    void CubicBezierCurve::setColorB(GLdouble b)
    {
        _color[2] = b;
    }

    CubicBezierCurve* CubicBezierCurve::join(CubicBezierCurve *other, GLboolean thisSide, GLboolean otherSide)
    {
        if (_attached[thisSide] || other->_attached[otherSide])
        {
            return GL_FALSE;
        }

        CubicBezierCurve* temp;
        ColumnMatrix<DCoordinate3> data(4);
        if(!thisSide && otherSide)
        {
            data[0] = other->_data[3];
            data[1] = 2*other->_data[3] - other->_data[2];
            data[2] = 2*_data[0] - _data[1];
            data[3] = _data[0];

            temp = new CubicBezierCurve(data);
        }
        else if(thisSide && !otherSide)
        {
            data[0] = other->_data[0];
            data[1] = 2*other->_data[0] - other->_data[1];
            data[2] = 2*_data[3]-_data[2];
            data[3] = _data[3];

            temp = new CubicBezierCurve(data);
        }
        else if(thisSide && otherSide)
        {
            data[0] = other->_data[3];
            data[1] = 2*other->_data[3] - other->_data[2];
            data[2] = 2*_data[3] - _data[2];
            data[3] = _data[3];

            temp = new CubicBezierCurve(data);
        }
        else
        {
            data[0] = other->_data[0];
            data[1] = 2*other->_data[0] - other->_data[1];
            data[2] = 2*_data[0] - data[1];
            data[3] = _data[0];

            temp = new CubicBezierCurve(data);
        }

        if(!temp)
        {
            return nullptr;
        }

        _attached[thisSide] = temp;
        _attached_side[thisSide] = 1;

        temp->_attached[1] = this;
        temp->_attached_side[1] = thisSide;

        temp->_attached[0] = other;
        temp->_attached_side[0] = otherSide;

        other->_attached[otherSide] = temp;
        other->_attached_side[otherSide] = 0;

        return temp;

    }

    void CubicBezierCurve::getConn(std::vector<CubicBezierCurve*>& ind, GLint ret[2])
    {
        for (GLuint i = 0; i < 2; i++)
        {
            auto index = std::find(ind.begin(), ind.end(), _attached[i]);
            if (index == ind.end())
                ret[i] = -1;
            else
                ret[i] = index - ind.begin();
        }
    }

    void CubicBezierCurve::setConn(std::vector<CubicBezierCurve*>& ind, GLint indexes[2])
    {
        for (GLuint i = 0; i < 2; i++)
        {
            if (indexes[i] == -1)
                _attached[i] = nullptr;
            else
                _attached[i] = ind[indexes[i]];
        }
    }
}
