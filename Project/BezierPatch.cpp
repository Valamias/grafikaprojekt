#include "BezierPatch.h"

using namespace cagd;
BicubicBezierPatch::BicubicBezierPatch():TensorProductSurface3(0.0,1.0,0.0,1.0,4,4)
{
    _toonOutlineColor.ResizeColumns(4);
    _toonOutlineColor[0] = 1.;
    _toonOutlineColor[1] = 1.;
    _toonOutlineColor[2] = 1.;
    _toonOutlineColor[3] = 1.;
}

GLboolean BicubicBezierPatch::UBlendingFunctionValues(
        GLdouble uknot, RowMatrix<GLdouble>& blendingvalues)const
{
    if(uknot < 0.0 || uknot > 1.0)
        return GL_FALSE;
    blendingvalues.ResizeColumns(4);
    GLdouble u = uknot, u2 = u * u, u3 = u2 * u, w = 1.0 - u, w2 = w * w, w3 = w2 * w;
    blendingvalues[0] = w3;
    blendingvalues[1] = 3.0 * w2 * u;
    blendingvalues[2] = 3.0 * w * u2;
    blendingvalues[3] = u3;
    return GL_TRUE;
}

GLboolean BicubicBezierPatch::VBlendingFunctionValues(
        GLdouble vknot, RowMatrix<GLdouble>& blendingvalues)const
{
    if(vknot < 0.0 || vknot > 1.0)
        return GL_FALSE;
    blendingvalues.ResizeColumns(4);
    GLdouble v = vknot, v2 = v * v, v3 = v2 * v, w = 1.0 - v, w2 = w * w, w3 = w2 * w;
    blendingvalues[0] = w3;
    blendingvalues[1] = 3.0 * w2 * v;
    blendingvalues[2] = 3.0 * w * v2;
    blendingvalues[3] = v3;
    return GL_TRUE;
}

GLboolean BicubicBezierPatch::CalculatePartialDerivatives(
        GLuint maximumorderofpartialderivatives,
        GLdouble u, GLdouble v, PartialDerivatives& pd)const
{
    if(u < 0.0 || u > 1.0 || v < 0.0 || v > 1.0 || maximumorderofpartialderivatives > 1)
        return GL_FALSE;
    //blendingfunctionvalues and their derivatives in u=direction
    RowMatrix<GLdouble> ublendingvalues(4), d1ublendingvalues(4);
    GLdouble u2 = u * u, u3 = u2 * u, wu = 1.0 - u, wu2= wu * wu, wu3 = wu2 * wu;
    ublendingvalues[0] = wu3;
    ublendingvalues[1] = 3.0 * wu2 * u;
    ublendingvalues[2] = 3.0 * wu * u2;
    ublendingvalues[3] = u3;
    d1ublendingvalues[0] = -3.0 * wu2;
    d1ublendingvalues[1] = -6.0 * wu * u + 3.0 * wu2;
    d1ublendingvalues[2] = -3.0 * u2 + 6.0 * wu * u;
    d1ublendingvalues[3] = 3.0 * u2;

    //blending function values and their derivatives in v=direction

    RowMatrix<GLdouble> vblendingvalues(4), d1vblendingvalues(4);
    GLdouble v2 = v * v, v3 = v2 * v, wv = 1.0 - v, wv2 = wv * wv, wv3 = wv2 * wv;
    //homework
    vblendingvalues[0] = wv3;
    vblendingvalues[1] = 3.0 * wv2 * v;
    vblendingvalues[2] = 3.0 * wv * v2;
    vblendingvalues[3] = v3;
    d1vblendingvalues[0] = -3.0 * wv2;
    d1vblendingvalues[1] = -6.0 * wv * v + 3.0 * wv2;
    d1vblendingvalues[2] = -3.0 * v2 + 6.0 * wv * v;
    d1vblendingvalues[3] = 3.0 * v2;

    pd.ResizeRows(2);
    pd.LoadNullVectors();
    for(GLuint row = 0; row < 4; ++row)
    {
        DCoordinate3 auxd0v, auxd1v;
        for(GLuint column = 0; column < 4; ++column)
        {
            auxd0v += _data(row,column) * vblendingvalues(column);
            auxd1v += _data(row,column) * d1vblendingvalues(column);
        }
        pd(0,0) += auxd0v * ublendingvalues(row);       //surfacepoint
        pd(1,0) += auxd0v * d1ublendingvalues(row);     //firstorderu=directionalpartialderivative
        pd(1,1) += auxd1v * ublendingvalues(row);       //firstorderv=directionalpartialderivative
    }
    return GL_TRUE;
}

void BicubicBezierPatch::setDataPoints(GLuint type, GLuint side, DCoordinate3 offset)
{
    switch(side)
    {
    case 0:
        _data(1,1) += offset;
        if (type == 1 || type == 3)
            _data(0,1) += offset;
        if (type == 2 || type == 3)
            _data(1,0) += offset;
        if (type == 3)
            _data(0,0) += offset;
        break;
    case 1:
        _data(1,2) += offset;
        if (type == 1 || type == 3)
            _data(0,2) += offset;
        if (type == 2 || type == 3)
            _data(1,3) += offset;
        if (type == 3)
            _data(0,3) += offset;
        break;
    case 2:
        _data(2,1) += offset;
        if (type == 1 || type == 3)
            _data(3,1) += offset;
        if (type == 2 || type == 3)
            _data(2,0) += offset;
        if (type == 3)
            _data(3,0) += offset;
        break;
    case 3:
        _data(2,2) += offset;
        if (type == 1 || type == 3)
            _data(3,2) += offset;
        if (type == 2 || type == 3)
            _data(2,3) += offset;
        if (type == 3)
            _data(3,3) += offset;
    }
}

void BicubicBezierPatch::setNorthWest(GLuint i, GLuint j, DCoordinate3 offset)
{
    if (i == 0)
    {
        if (j == 0)
        {
            if (_attached[1] || _attached[4] || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 3)%4]))
            {
                _data(1,0) += offset;
            }
            if (_attached[0] || _attached[4] || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 1)%4]))
            {
                _data(0,1) += offset;
            }
            if ((_attached[4] && (_attached[0] || _attached[1]))
                    || (_attached[0] && _attached[1])
                    || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 3)%4])
                    || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 1)%4]))
            {
                _data(1,1) += offset;
            }
            if (_attached[0])
            {
                _attached[0]->setDataPointInner((_attached_side[0] == 0 || _attached_side[0] == 3)*3, (_attached_side[0] > 1)*3, offset);
            }
            if (_attached[1])
            {
                _attached[1]->setDataPointInner((_attached_side[1] > 1)*3, (_attached_side[1] == 1 || _attached_side[1] == 2)*3, offset);
            }
            if (_attached[4])
            {
                _attached[4]->setDataPointInner(((_attached_side[4]-4) < 2)*3, ((_attached_side[4]-4)%2 < 2)*3, offset);
            }
        }
        else
        {
            if ((_attached[4] && (_attached[0] || _attached[1]))
                    || (_attached[0] && _attached[1])
                    || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 3)%4])
                    || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 1)%4]))
            {
                _data(1,1) += offset;
            }
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(1, 0, offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(0, 2, offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(2, 3, offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(3, 1, offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(3, 1, -offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(1, 0, -offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(0, 2, -offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }

            if (_attached[4])
            {
                switch(_attached_side[4] - 4)
                {
                case 0:
                    _attached[4]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[4]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[4]->setDataPointInner(2, 0, -offset);
                    break;
                case 3:
                    _attached[4]->setDataPointInner(3, 2, -offset);
                    break;
                }
            }
        }
    }
    else
    {
        if (j == 0)
        {
            if ((_attached[4] && (_attached[0] || _attached[1]))
                    || (_attached[0] && _attached[1])
                    || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 3)%4])
                    || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 1)%4]))
            {
                _data(1,1) += offset;
            }
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(0, 2, -offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(2, 0, offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(0, 1, offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(1, 3, offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(3, 2, offset);
                    break;
                }
            }

            if (_attached[4])
            {
                switch(_attached_side[4] - 4)
                {
                case 0:
                    _attached[4]->setDataPointInner(1, 0, -offset);
                    break;
                case 1:
                    _attached[4]->setDataPointInner(0, 2, -offset);
                    break;
                case 2:
                    _attached[4]->setDataPointInner(3, 1, -offset);
                    break;
                case 3:
                    _attached[4]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }
        }
        else{
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(1, 1, -offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(1, 2, -offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(2, 1, -offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(2, 2, -offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(2, 1, -offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(1, 1, -offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(1, 2, -offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(2, 2, -offset);
                    break;
                }
            }
        }
    }
}

void BicubicBezierPatch::setNorthEast(GLuint i, GLuint j, DCoordinate3 offset)
{
    if (i == 0)
    {
        if (j == 3)
        {
            if (_attached[1] || _attached[5] || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 1)%4]))
            {
                _data(1,3) += offset;
            }
            if (_attached[2] || _attached[5] || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 3)%4]))
            {
                _data(0,2) += offset;
            }
            if ((_attached[5] && (_attached[2] || _attached[1]))
                    || (_attached[2] && _attached[1])
                    || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 3)%4])
                    || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 1)%4]))
            {
                _data(1,2) += offset;
            }
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(3, 0, offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(0, 0, offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(0, 3, offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(3, 3, offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(0, 0, offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(0, 3, offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(3, 3, offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(3, 0, offset);
                    break;
                }
            }

            if (_attached[5])
            {
                switch(_attached_side[5] - 4)
                {
                case 0:
                    _attached[5]->setDataPointInner(0, 0, offset);
                    break;
                case 1:
                    _attached[5]->setDataPointInner(0, 3, offset);
                    break;
                case 2:
                    _attached[5]->setDataPointInner(3, 0, offset);
                    break;
                case 3:
                    _attached[5]->setDataPointInner(3, 3, offset);
                    break;
                }
            }
        }
        else
        {
            if ((_attached[5] && (_attached[2] || _attached[1])) || (_attached[2] && _attached[1])
                    || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 3)%4])
                    || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 1)%4]))
            {
                _data(1,2) += offset;
            }
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(2, 0, offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(0, 1, offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(1, 3, offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(3, 2, offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(2, 0, -offset);
                    break;
                }
            }

            if (_attached[5])
            {
                switch(_attached_side[5] - 4)
                {
                case 0:
                    _attached[5]->setDataPointInner(1, 0, -offset);
                    break;
                case 1:
                    _attached[5]->setDataPointInner(0, 3, -offset);
                    break;
                case 2:
                    _attached[5]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[5]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }
        }
    }
    else
    {
        if (j == 3)
        {
            if ((_attached[4] && (_attached[2] || _attached[1])) || (_attached[2] && _attached[1])
                    || (_attached[1] && _attached[1]->_attached[(_attached_side[1] + 3)%4])
                    || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 1)%4]))
            {
                _data(1,2) += offset;
            }
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(3, 1, -offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(1, 0, -offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(0, 2, -offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(1, 0, offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(0, 2, offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(2, 3, offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(3, 1, offset);
                    break;
                }
            }

            if (_attached[5])
            {
                switch(_attached_side[5] - 4)
                {
                case 0:
                    _attached[5]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[5]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[5]->setDataPointInner(0, 3, -offset);
                    break;
                case 3:
                    _attached[5]->setDataPointInner(3, 2, -offset);
                    break;
                }
            }
        }
        else
        {
            if (_attached[1])
            {
                switch(_attached_side[1])
                {
                case 0:
                    _attached[1]->setDataPointInner(2, 1, -offset);
                    break;
                case 1:
                    _attached[1]->setDataPointInner(1, 1, -offset);
                    break;
                case 2:
                    _attached[1]->setDataPointInner(1, 2, -offset);
                    break;
                case 3:
                    _attached[1]->setDataPointInner(2, 2, -offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(1, 1, -offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(1, 2, -offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(2, 2, -offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(2, 1, -offset);
                    break;
                }
            }
        }
    }
}

void BicubicBezierPatch::setSouthWest(GLuint i, GLuint j, DCoordinate3 offset)
{
    if (i == 3)
    {
        if (j == 0)
        {
            if (_attached[3] || _attached[6] || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 1)%4]))
            {
                _data(2,0) += offset;
            }
            if (_attached[0] || _attached[6] || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 3)%4]))
            {
                _data(3,1) += offset;
            }
            if ((_attached[6] && (_attached[0] || _attached[3])) || (_attached[0] && _attached[3])
                    || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 1)%4])
                    || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 3)%4]))
            {
                _data(2,1) += offset;
            }
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(3, 0, offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(0, 0, offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(0, 3, offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(3, 3, offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(0, 0, offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(0, 3, offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(3, 3, offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(3, 0, offset);
                    break;
                }
            }

            if (_attached[6])
            {
                switch(_attached_side[6] - 4)
                {
                case 0:
                    _attached[6]->setDataPointInner(0, 0, offset);
                    break;
                case 1:
                    _attached[6]->setDataPointInner(0, 3, offset);
                    break;
                case 2:
                    _attached[6]->setDataPointInner(3, 0, offset);
                    break;
                case 3:
                    _attached[6]->setDataPointInner(3, 3, offset);
                    break;
                }
            }
        }
        else
        {
            if ((_attached[6] && (_attached[0] || _attached[3])) || (_attached[0] && _attached[3])
                    || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 1)%4])
                    || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 3)%4]))
            {
                _data(2,1) += offset;
            }
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(2, 0, offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(0, 1, offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(1, 3, offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(3, 2, offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(3, 0, -offset);
                    break;
                }
            }

            if (_attached[6])
            {
                switch(_attached_side[6] - 4)
                {
                case 0:
                    _attached[6]->setDataPointInner(1, 0, -offset);
                    break;
                case 1:
                    _attached[6]->setDataPointInner(0, 3, -offset);
                    break;
                case 2:
                    _attached[6]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[6]->setDataPointInner(1, 3, -offset);
                    break;
                }
            }
        }
    }
    else
    {
        if (j == 0)
        {
            if ((_attached[6] && (_attached[0] || _attached[3])) || (_attached[0] && _attached[3])
                    || (_attached[0] && _attached[0]->_attached[(_attached_side[0] + 1)%4])
                    || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 3)%4]))
            {
                _data(2,1) += offset;
            }
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(3, 1, -offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(1, 0, -offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(0, 2, -offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(1, 0, offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(0, 2, offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(2, 3, offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(3, 1, offset);
                    break;
                }
            }

            if (_attached[6])
            {
                switch(_attached_side[6] - 4)
                {
                case 0:
                    _attached[6]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[6]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[6]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[6]->setDataPointInner(2, 0, -offset);
                    break;
                }
            }
        }
        else
        {
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(2, 1, -offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(1, 1, -offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(1, 2, -offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(2, 2, -offset);
                    break;
                }
            }

            if (_attached[0])
            {
                switch(_attached_side[0])
                {
                case 0:
                    _attached[0]->setDataPointInner(1, 1, -offset);
                    break;
                case 1:
                    _attached[0]->setDataPointInner(1, 2, -offset);
                    break;
                case 2:
                    _attached[0]->setDataPointInner(2, 2, -offset);
                    break;
                case 3:
                    _attached[0]->setDataPointInner(2, 1, -offset);
                    break;
                }
            }
        }
    }
}

void BicubicBezierPatch::setSouthEast(GLuint i, GLuint j, DCoordinate3 offset)
{
    if (i == 3)
    {
        if (j == 3)
        {
            if (_attached[3] || _attached[7] || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 3)%4]))
            {
                _data(2,3) += offset;
            }
            if (_attached[2] || _attached[7] || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 1)%4]))
            {
                _data(3,2) += offset;
            }
            if ((_attached[7] && (_attached[2] || _attached[3])) || (_attached[2] && _attached[3])
                    || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 3)%4])
                    || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 1)%4]))
            {
                _data(2,2) += offset;
            }

            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(0, 0, offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(0, 3, offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(3, 3, offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(3, 0, offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(3, 0, offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(0, 0, offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(0, 3, offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(3, 3, offset);
                    break;
                }
            }

            if (_attached[7])
            {
                switch(_attached_side[7] - 4)
                {
                case 0:
                    _attached[7]->setDataPointInner(0, 0, offset);
                    break;
                case 1:
                    _attached[7]->setDataPointInner(0, 3, offset);
                    break;
                case 2:
                    _attached[7]->setDataPointInner(3, 0, offset);
                    break;
                case 3:
                    _attached[7]->setDataPointInner(3, 3, offset);
                    break;
                }
            }
        }
        else
        {
            if ((_attached[7] && (_attached[2] || _attached[3])) || (_attached[2] && _attached[3])
                    || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 3)%4])
                    || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 1)%4]))
            {
                _data(2,2) += offset;
            }
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(1, 0, offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(0, 2, offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(2, 3, offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(3, 1, offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(3, 1, -offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(1, 0, -offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(0, 2, -offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }

            if (_attached[7])
            {
                switch(_attached_side[7] - 4)
                {
                case 0:
                    _attached[7]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[7]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[7]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[7]->setDataPointInner(2, 0, -offset);
                    break;
                }
            }
        }
    }
    else
    {
        if (j == 3)
        {
            if ((_attached[7] && (_attached[2] || _attached[3])) || (_attached[2] && _attached[3])
                    || (_attached[2] && _attached[2]->_attached[(_attached_side[2] + 3)%4])
                    || (_attached[3] && _attached[3]->_attached[(_attached_side[3] + 1)%4]))
            {
                _data(2,2) += offset;
            }
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(0, 1, -offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(1, 3, -offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(3, 2, -offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(2, 0, -offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(2, 0, offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(0, 1, offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(1, 3, offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(3, 2, offset);
                    break;
                }
            }

            if (_attached[7])
            {
                switch(_attached_side[7] - 4)
                {
                case 0:
                    _attached[7]->setDataPointInner(1, 0, -offset);
                    break;
                case 1:
                    _attached[7]->setDataPointInner(0, 2, -offset);
                    break;
                case 2:
                    _attached[7]->setDataPointInner(3, 1, -offset);
                    break;
                case 3:
                    _attached[7]->setDataPointInner(2, 3, -offset);
                    break;
                }
            }
        }
        else
        {
            if (_attached[3])
            {
                switch(_attached_side[3])
                {
                case 0:
                    _attached[3]->setDataPointInner(1, 1, -offset);
                    break;
                case 1:
                    _attached[3]->setDataPointInner(1, 2, -offset);
                    break;
                case 2:
                    _attached[3]->setDataPointInner(2, 2, -offset);
                    break;
                case 3:
                    _attached[3]->setDataPointInner(2, 1, -offset);
                    break;
                }
            }

            if (_attached[2])
            {
                switch(_attached_side[2])
                {
                case 0:
                    _attached[2]->setDataPointInner(2, 1, -offset);
                    break;
                case 1:
                    _attached[2]->setDataPointInner(1, 1, -offset);
                    break;
                case 2:
                    _attached[2]->setDataPointInner(1, 2, -offset);
                    break;
                case 3:
                    _attached[2]->setDataPointInner(2, 2, -offset);
                    break;
                }
            }
        }
    }
}

void BicubicBezierPatch::setDataPointInner(GLuint i, GLuint j, DCoordinate3 offset)
{
    if (i < 0 || i > 3 || j < 0 || j > 3)
        return;
    if (i < 2)
    {
        if (j < 2)
        {
            if (_modified[0])
                return;
            _modified[0] = true;
            _data(i,j) += offset;
            setNorthWest(i,j,offset);
        }
        else
        {
            if (_modified[1])
                return;
            _modified[1] = true;
            _data(i,j) += offset;
            setNorthEast(i,j,offset);
        }
    }
    else
    {
        if (j < 2)
        {
            if (_modified[2])
                return;
            _modified[2] = true;
            _data(i,j) += offset;
            setSouthWest(i,j,offset);
        }
        else
        {
            if (_modified[3])
                return;
            _modified[3] = true;
            _data(i,j) += offset;
            setSouthEast(i,j,offset);
        }
    }
}

GLboolean BicubicBezierPatch::setDataPoint(GLuint i, GLuint j, DCoordinate3 point)
{
    if (i > 3 || j > 3)
        return GL_FALSE;
    if (point == _data(i,j))
        return GL_TRUE;

    setDataPointInner(i,j,point - _data(i,j));
    unSet();

    return GL_TRUE;
}

void BicubicBezierPatch::unSet()
{
    bool isACheck = false;
    for (GLuint i=0; i<4; i++)
    {
        if (_modified[i])
            isACheck = true;
    }
    if (!isACheck)
        return;
    for (GLuint i=0; i<4; i++)
    {
        _modified[i] = false;
    }
    for (int i = 0; i< 8; i++)
    {
        if (_attached[i])
        {
            _attached[i]->unSet();
        }
    }
}

void BicubicBezierPatch::unSetRender()
{
    bool isACheck = false;
    for (GLuint i=0; i<4; i++)
    {
        if (_modified[i])
            isACheck = true;
    }
    if (!isACheck)
        return;
    for (GLuint i=0; i<4; i++)
    {
        _modified[i] = false;
    }
    updateImageForRendering();
    for (int i = 0; i< 8; i++)
    {
        if (_attached[i])
        {
            _attached[i]->unSetRender();
        }
    }
}

void BicubicBezierPatch::Set()
{
    if (_modified[0])
        return;
    _modified[0] = true;
    for (int i = 0; i< 8; i++)
    {
        if (_attached[i])
        {
            _attached[i]->Set();
        }
    }
}

GLboolean BicubicBezierPatch::setDataPointX(GLuint i, GLuint j, GLdouble x)
{
    DCoordinate3 newX = _data(i,j);
    newX.x() = x;
    return setDataPoint(i,j,newX);
}

GLboolean BicubicBezierPatch::setDataPointY(GLuint i, GLuint j, GLdouble y)
{
    DCoordinate3 newY = _data(i,j);
    newY.y() = y;
    return setDataPoint(i,j,newY);
}

GLboolean BicubicBezierPatch::setDataPointZ(GLuint i, GLuint j, GLdouble z)
{
    DCoordinate3 newZ = _data(i,j);
    newZ.z() = z;
    return setDataPoint(i,j,newZ);
}

void BicubicBezierPatch::attach(GLuint side1, GLuint side2, BicubicBezierPatch* patch)
{
    _attached[side1] = patch;
    _attached_side[side1] = side2;
}

void BicubicBezierPatch::detach(GLuint side)
{
    _attached[side] = nullptr;
}

BicubicBezierPatch::~BicubicBezierPatch()
{
    for (GLuint i=0; i < 8; i++)
    {
        if (_attached[i])
        {
            _attached[i]->detach(_attached_side[i]);
            _attached[i] = nullptr;
        }
    }
    DeleteVertexBufferObjectsOfData();

    if (_image)
    {
        _image->DeleteVertexBufferObjects();
        delete _image;
        _image = nullptr;
    }

    if (_UIsoParametricLines)
    {
        delete _UIsoParametricLines;
        _UIsoParametricLines = nullptr;
    }

    if(_VIsoParametricLines)
    {
        delete _VIsoParametricLines;
        _VIsoParametricLines = nullptr;
    }
}

BicubicBezierPatch* BicubicBezierPatch::extend(GLuint side)
{
    if (_attached[side])
        return nullptr;
    BicubicBezierPatch* extension;
    extension = new BicubicBezierPatch();
    if (!extension)
        return nullptr;

    GLint curr;
    GLint next;
    GLuint otherside;

    if (side%2 == 1)
    {
        for (GLuint i = 0; i < 4; i++)
        {
            curr = (!(side/2))*3;
            next = 1 - (!(side/2))*2;
            extension->_data(curr, i) = _data((side/2)*3, i);

            extension->_data(curr + next, i) = 2 * extension->_data(curr, i) - _data(1 + (side/2), i);
            curr += next;
            extension->_data(curr + next, i) = 2 * extension->_data(curr, i) - extension->_data(curr - next, i);
            curr += next;
            extension->_data(curr + next, i) = 2 * extension->_data(curr, i) - extension->_data(curr - next, i);
        }

        otherside = 1 + 2*(side<1);
    }
    else
    {
        for (GLuint i = 0; i < 4; i++)
        {
            curr = (!(side/2))*3;
            next = 1 - (!(side/2))*2;
            extension->_data(i, curr) = _data(i, (side/2)*3);

            extension->_data(i, curr + next) = 2 * extension->_data(i, curr) - _data(i, 1 + (side/2));
            curr += next;
            extension->_data(i, curr + next) = 2 * extension->_data(i, curr) - extension->_data(i, curr - next);
            curr += next;
            extension->_data(i, curr + next) = 2 * extension->_data(i, curr) - extension->_data(i, curr - next);
        }
        otherside = 2*(side<1);
    }

    _attached[side] = extension;
    _attached_side[side] = otherside;

    extension->_attached[otherside] = this;
    extension->_attached_side[otherside] = side;

    return extension;
}

GLboolean BicubicBezierPatch::merge(BicubicBezierPatch* other, GLuint thisSide, GLuint otherSide)
{
    if (_attached[thisSide] || other->_attached[otherSide])
        return GL_FALSE;

    GLuint oldPoints1[4][2][2];
    GLuint oldPoints2[4][2][2];
    DCoordinate3 originals1[4];
    DCoordinate3 originals2[4];

    if (thisSide%2 == 1)
    {
        for (GLuint i = 0; i < 4; i++)
        {
            originals1[i] = _data(1 + (thisSide == 3), i);
            oldPoints1[i][0][0] = (thisSide == 3)*3;
            oldPoints1[i][0][1] = i;
            oldPoints1[i][1][0] = 1 + (thisSide == 3);
            oldPoints1[i][1][1] = i;
        }
    }
    else
    {
        for (GLuint i = 0; i < 4; i++)
        {
            originals1[i] = _data(i, 1 + (thisSide == 2));
            oldPoints1[i][0][0] = i;
            oldPoints1[i][0][1] = 3 * (thisSide == 2);
            oldPoints1[i][1][0] = i;
            oldPoints1[i][1][1] = 1 + (thisSide == 2);
        }
    }

    if (otherSide%2 == 1)
    {
        for (GLuint i = 0; i < 4; i++)
        {
            originals2[i] = other->_data(1 + (otherSide == 3), i);
            oldPoints2[i][0][0] = (otherSide == 3)*3;
            oldPoints2[i][0][1] = i;
            oldPoints2[i][1][0] = 1 + (otherSide == 3);
            oldPoints2[i][1][1] = i;
        }
    }
    else
    {
        for (GLuint i = 0; i < 4; i++)
        {
            originals2[i] = other->_data(i, 1 + (otherSide == 2));
            oldPoints2[i][0][0] = i;
            oldPoints2[i][0][1] = 3 * (otherSide == 2);
            oldPoints2[i][1][0] = i;
            oldPoints2[i][1][1] = 1 + (otherSide == 2);
        }
    }

    // 0 - 1,2
    // 1 - 0,3
    // 2 - 0,3
    // 3 - 1,2

    if (((thisSide == 0 || thisSide == 3) && (otherSide == 1 || otherSide == 2))
            || ((thisSide == 1 || thisSide == 2) && (otherSide == 0 || otherSide == 3)))
    {
        for (GLint i = -1; i < 3; i++)
        {
            const GLint j = (4 + i)%4;
            setDataPoint(oldPoints1[j][0][0], oldPoints1[j][0][1], (originals1[j] + originals2[j])/2);
            other->setDataPoint(oldPoints2[j][0][0], oldPoints2[j][0][1], (originals1[j] + originals2[j])/2);
        }
    }
    else
    {
        for (GLint i = -1; i < 3; i++)
        {
            const GLint j = (4 + i)%4;
            setDataPoint(oldPoints1[j][0][0], oldPoints1[j][0][1], (originals1[j] + originals2[3-j])/2);
            other->setDataPoint(oldPoints2[j][0][0], oldPoints2[j][0][1], (originals1[3-j] + originals2[j])/2);
        }
    }

    for (GLint i = -1; i < 4; i++)
    {
        const GLint j = (4 + i)%4;
        setDataPoint(oldPoints1[j][1][0], oldPoints1[j][1][1], originals1[j]);
        other->setDataPoint(oldPoints2[j][1][0], oldPoints2[j][1][1], originals2[j]);
    }


    _attached[thisSide] = other;
    _attached_side[thisSide] = otherSide;

    other->_attached[otherSide] = this;
    other->_attached_side[otherSide] = thisSide;

    return GL_TRUE;
}

GLboolean BicubicBezierPatch::updateImageForRendering()
{
    DeleteVertexBufferObjectsOfData();

    if (!UpdateVertexBufferObjectsOfData())
    {
        std::cout << "Couln't update patch!" << std::endl;
        return false;
    }
    if (_image)
    {
        _image -> DeleteVertexBufferObjects();
        delete _image;
        _image = nullptr;
    }

    _image = GenerateImage(_u_div_point_count,_v_div_point_count);

    if (!_image)
    {
        std::cout << "Couln't create curve image!" << std::endl;
        return false;
    }

    if (!_image->UpdateVertexBufferObjects())
    {
        std::cout << "Couln't update curve vertexbuffer objects!" << std::endl;
        return false;
    }
    return true;
}

GLboolean BicubicBezierPatch::updateAllInvolvedForRendering()
{
    Set();
    unSetRender();
    return GL_TRUE;
}

GLboolean BicubicBezierPatch::updateUIsoParametricLinesForRendering()
{
    if (_UIsoParametricLines)
        delete _UIsoParametricLines;
    _UIsoParametricLines = GenerateUIsoparametricLines(_u_line_count, 1, _u_div_point_count);
    if (!_UIsoParametricLines)
        return GL_FALSE;
    for (GLuint j=0; j<_UIsoParametricLines->GetColumnCount(); j++)
    {
        if (!(*_UIsoParametricLines)[j]->UpdateVertexBufferObjects())
            return GL_FALSE;
    }
    return GL_TRUE;
}

GLboolean BicubicBezierPatch::updateVIsoParametricLinesForRendering()
{
    if (_VIsoParametricLines)
        delete _VIsoParametricLines;
    _VIsoParametricLines = GenerateVIsoparametricLines(_v_line_count, 1, _v_div_point_count);
    if (!_VIsoParametricLines)
        return GL_FALSE;
    for (GLuint j=0; j<_VIsoParametricLines->GetColumnCount(); j++)
    {
        if (!(*_VIsoParametricLines)[j]->UpdateVertexBufferObjects())
            return GL_FALSE;
    }
    return GL_TRUE;
}

GLboolean BicubicBezierPatch::render()
{
    if (!_image)
        updateImageForRendering();
    return _image->Render();
}

GLboolean BicubicBezierPatch::renderNormalVectors(GLdouble size)
{
    return _image->RenderNormalVectors(size);
}

GLboolean BicubicBezierPatch::renderUIsoParametricLines(GLuint der, GLenum mode)
{
    for (GLuint j=0; j<_UIsoParametricLines->GetColumnCount(); j++)
    {
        if (!(*_UIsoParametricLines)[j]->RenderDerivatives(der, mode))
            return GL_FALSE;
    }
    return GL_TRUE;
}

GLboolean BicubicBezierPatch::renderVIsoParametricLines(GLuint der, GLenum mode)
{
    for (GLuint j=0; j<_VIsoParametricLines->GetColumnCount(); j++)
    {
        if (!(*_VIsoParametricLines)[j]->RenderDerivatives(der, mode))
            return GL_FALSE;
    }
    return GL_TRUE;
}

void BicubicBezierPatch::set_u_div_point_count(GLuint count)
{
    _u_div_point_count = count;
}

void BicubicBezierPatch::set_v_div_point_count(GLuint count)
{
    _v_div_point_count = count;
}

void BicubicBezierPatch::set_u_line_count(GLuint count)
{
    _u_line_count = count;
}

void BicubicBezierPatch::set_v_line_count(GLuint count)
{
    _v_line_count = count;
}

GLuint BicubicBezierPatch::get_u_div_point_count() const
{
    return _u_div_point_count;
}

GLuint BicubicBezierPatch::get_v_div_point_count() const
{
    return _v_div_point_count;
}

GLuint BicubicBezierPatch::get_u_line_count() const
{
    return _u_line_count;
}

GLuint BicubicBezierPatch::get_v_line_count() const
{
    return _v_line_count;
}

void BicubicBezierPatch::translatePatch(DCoordinate3 offset)
{
    DCoordinate3 old[4][4];
    for (GLint i = 0; i < 4; i++)
    {
        for (GLint j = 0; j < 4; j++)
        {
            old[i][j] = _data(i,j) + offset;
        }
    }
    for (GLint i = -1; i < 3; i++)
    {
        for (GLint j = -1; j < 3; j++)
        {
            setDataPoint((i+4)%4, (j+4)%4, old[(i+4)%4][(j+4)%4]);
        }
    }
}

void BicubicBezierPatch::innertranslateWhole(DCoordinate3 offset)
{
    if (_modified[0])
        return;
    _modified[0] = true;
    for (GLuint i = 0; i < 4; i++)
    {
        for (GLuint j = 0; j < 4; j++)
        {
            _data(i,j) += offset;
        }
    }
    for (GLuint i = 0; i < 8; i++)
    {
        if (_attached[i])
        {
            _attached[i]->innertranslateWhole(offset);
        }
    }
}

void BicubicBezierPatch::translateWhole(DCoordinate3 offset)
{
    innertranslateWhole(offset);
    unSet();
}

void BicubicBezierPatch::setColorR(GLdouble r)
{
    _color[0] = r;
}
void BicubicBezierPatch::setColorG(GLdouble g)
{
    _color[1] = g;
}
void BicubicBezierPatch::setColorB(GLdouble b)
{
    _color[2] = b;
}

GLdouble BicubicBezierPatch::getColorR() const
{
    return _color[0];
}
GLdouble BicubicBezierPatch::getColorG() const
{
    return _color[1];
}
GLdouble BicubicBezierPatch::getColorB() const
{
    return _color[2];
}

void    BicubicBezierPatch::setMaterial(GLuint mat)
{
    _mat = mat;
}

GLuint  BicubicBezierPatch::getMaterial() const
{
    return _mat;
}

BicubicBezierPatch* BicubicBezierPatch::join(BicubicBezierPatch* other, GLuint thisSide, GLuint otherSide)
{
    if (_attached[thisSide] || other->_attached[otherSide])
    {
        return nullptr;
    }
    BicubicBezierPatch* join;
    join = new BicubicBezierPatch();
    if (!join)
    {
        return nullptr;
    }

    if (thisSide == 0)
    {
        if (otherSide == 0)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(i,0,ertek);
                join->SetData(0,i,ertek);

                other->GetData(i,0,ertek);
                join->SetData(3,3-i,ertek);

                GetData(i,0,ertek);
                GetData(i,1,ertek2);
                join->SetData(1,i,2*ertek-ertek2);


                other->GetData(i,0,ertek);
                other->GetData(i,1,ertek2);
                join->SetData(2,3-i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
        else if (otherSide == 1)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {

                GetData(i,0,ertek);
                join->SetData(0,i,ertek);

                other->GetData(0,i,ertek);

                join->SetData(3,i,ertek);

                GetData(i,0,ertek);
                GetData(i,1,ertek2);
                join->SetData(1,i,2*ertek-ertek2);

                other->GetData(0,i,ertek);
                other->GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
        else if (otherSide == 2)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(i,0,ertek);
                join->SetData(i,3,ertek);

                other->GetData(i,3,ertek);
                join->SetData(i,0,ertek);


                GetData(i,0,ertek);
                GetData(i,1,ertek2);
                join->SetData(i,2,2*ertek-ertek2);

                other->GetData(i,3,ertek);
                other->GetData(i,2,ertek2);
                join->SetData(i,1,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 2;

            join->_attached[2] = this;
            join->_attached_side[2] = thisSide;

            join->_attached[0] = other;
            join->_attached_side[0] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 0;

        }
        else
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(3-i,0,ertek);
                join->SetData(3,i,ertek);

                other->GetData(3,i,ertek);
                join->SetData(0,i,ertek);

                GetData(3-i,0,ertek);
                GetData(3-i,1,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

                other->GetData(3,i,ertek);
                other->GetData(2,i,ertek2);
                join->SetData(1,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 3;

            join->_attached[3] = this;
            join->_attached_side[3] = thisSide;

            join->_attached[1] = other;
            join->_attached_side[1] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 1;

        }
    }
    else if(thisSide == 1)
    {
        if (otherSide == 0)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(0,i,ertek);
                join->SetData(3,i,ertek);

                other->GetData(i,0,ertek);
                join->SetData(0,i,ertek);

                GetData(0,i,ertek);
                GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

                other->GetData(i,0,ertek);
                other->GetData(i,1,ertek2);

                join->SetData(1,i,2*ertek-ertek2);
            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 3;

            join->_attached[3] = this;
            join->_attached_side[3] = thisSide;

            join->_attached[1] = other;
            join->_attached_side[1] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 1;

        }
        else if (otherSide == 1)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(0,i,ertek);
                join->SetData(0,3-i,ertek);

                other->GetData(0,i,ertek);
                join->SetData(3,i,ertek);

                GetData(0,i,ertek);
                GetData(1,i,ertek2);
                join->SetData(1,3-i,2*ertek-ertek2);


                other->GetData(0,i,ertek);
                other->GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
        else if (otherSide == 2)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(0,i,ertek);
                join->SetData(3,i,ertek);

                other->GetData(i,3,ertek);
                join->SetData(0,3-i,ertek);

                GetData(0,i,ertek);
                GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

                other->GetData(i,3,ertek);
                other->GetData(i,2,ertek2);
                join->SetData(1,3-i,2*ertek-ertek2);
            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 3;

            join->_attached[3] = this;
            join->_attached_side[3] = thisSide;

            join->_attached[1] = other;
            join->_attached_side[1] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 1;

        }
        else
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(0,i,ertek);
                join->SetData(3,i,ertek);

                other->GetData(3,i,ertek);
                join->SetData(0,i,ertek);

                GetData(0,i,ertek);
                GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

                other->GetData(3,i,ertek);
                other->GetData(2,i,ertek2);
                join->SetData(1,i,2*ertek-ertek2);


            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 3;

            join->_attached[3] = this;
            join->_attached_side[3] = thisSide;

            join->_attached[1] = other;
            join->_attached_side[1] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 1;

        }
    }
    else if(thisSide == 2)
    {
        if (otherSide == 0)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(i,3,ertek);
                join->SetData(i,0,ertek);

                other->GetData(i,0,ertek);
                join->SetData(i,3,ertek);

                GetData(i,3,ertek);
                GetData(i,2,ertek2);
                join->SetData(i,1,2*ertek-ertek2);

                other->GetData(i,0,ertek);
                other->GetData(i,1,ertek2);

                join->SetData(i,2,2*ertek-ertek2);
            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 0;

            join->_attached[0] = this;
            join->_attached_side[0] = thisSide;

            join->_attached[2] = other;
            join->_attached_side[2] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 2;

        }
        else if (otherSide == 1)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(i,3,ertek);
                join->SetData(0,3-i,ertek);

                other->GetData(0,i,ertek);
                join->SetData(3,i,ertek);


                GetData(i,3,ertek);
                GetData(i,2,ertek2);
                join->SetData(1,3-i,2*ertek-ertek2);

                other->GetData(0,i,ertek);
                other->GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
        else if (otherSide == 2)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(i,3,ertek);
                join->SetData(i,0,ertek);

                other->GetData(i,3,ertek);
                join->SetData(3-i,3,ertek);


                GetData(i,3,ertek);
                GetData(i,2,ertek2);
                join->SetData(i,1,2*ertek-ertek2);


                other->GetData(i,3,ertek);
                other->GetData(i,2,ertek2);
                join->SetData(3-i,2,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 0;

            join->_attached[0] = this;
            join->_attached_side[0] = thisSide;

            join->_attached[2] = other;
            join->_attached_side[2] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 2;

        }
        else
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(i,3,ertek);
                join->SetData(3,i,ertek);

                other->GetData(3,i,ertek);
                join->SetData(0,i,ertek);


                GetData(i,3,ertek);
                GetData(i,2,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

                other->GetData(3,i,ertek);
                other->GetData(2,i,ertek2);
                join->SetData(1,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 3;

            join->_attached[3] = this;
            join->_attached_side[3] = thisSide;

            join->_attached[1] = other;
            join->_attached_side[1] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 1;

        }
    }
    else
    {
        if (otherSide == 0)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(3,i,ertek);
                join->SetData(3-i,0,ertek);

                other->GetData(i,0,ertek);
                join->SetData(i,3,ertek);

                GetData(3,i,ertek);
                GetData(2,i,ertek2);
                join->SetData(3-i,1,2*ertek2-ertek);

                other->GetData(i,0,ertek);
                other->GetData(i,1,ertek2);
                join->SetData(i,2,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 0;

            join->_attached[0] = this;
            join->_attached_side[0] = thisSide;

            join->_attached[2] = other;
            join->_attached_side[2] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 2;

        }
        else if (otherSide == 1)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(3,i,ertek);
                join->SetData(0,i,ertek);

                other->GetData(0,i,ertek);
                join->SetData(3,i,ertek);

                GetData(3,i,ertek);
                GetData(2,i,ertek2);
                join->SetData(1,i,2*ertek-ertek2);

                other->GetData(0,i,ertek);
                other->GetData(1,i,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
        else if (otherSide == 2)
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(3,i,ertek);
                join->SetData(0,i,ertek);

                other->GetData(i,3,ertek);
                join->SetData(3,i,ertek);

                GetData(3,i,ertek);
                GetData(2,i,ertek2);
                join->SetData(1,i,2*ertek-ertek2);

                other->GetData(i,3,ertek);
                other->GetData(i,2,ertek2);
                join->SetData(2,i,2*ertek-ertek2);

            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
        else
        {
            DCoordinate3 ertek;
            DCoordinate3 ertek2;

            for(int i = 0; i < 4; ++i)
            {
                GetData(3,i,ertek);
                join->SetData(0,i,ertek);

                other->GetData(3,i,ertek);
                join->SetData(3,3-i,ertek);

                GetData(3,i,ertek);
                GetData(2,i,ertek2);
                join->SetData(1,i,2*ertek-ertek2);

                other->GetData(3,i,ertek);
                other->GetData(2,i,ertek2);
                join->SetData(2,3-i,2*ertek-ertek2);


            }

            join->UpdateVertexBufferObjectsOfData();

            _attached[thisSide] = join;
            _attached_side[thisSide] = 1;

            join->_attached[1] = this;
            join->_attached_side[1] = thisSide;

            join->_attached[3] = other;
            join->_attached_side[3] = otherSide;

            other->_attached[otherSide] = join;
            other->_attached_side[otherSide] = 3;

        }
    }



    return join;
}

void BicubicBezierPatch::rotX(GLint rot, GLuint x[4][4])
{
    rot = (rot+4)%4;
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            switch(rot)
            {
            case 0:
                x[i][j] = i;
                break;
            case 1:
                x[i][j] = 3 - j;
                break;
            case 2:
                x[i][j] = 3 - i;
                break;
            case 3:
                x[i][j] = j;
            }
        }
    }
}

void BicubicBezierPatch::rotY(GLint rot, GLuint x[4][4])
{
    rot = (rot+4)%4;
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            switch(rot)
            {
            case 0:
                x[i][j] = j;
                break;
            case 1:
                x[i][j] = i;
                break;
            case 2:
                x[i][j] = 3 - j;
                break;
            case 3:
                x[i][j] = 3 - i;
            }
        }
    }
}

BicubicBezierPatch* BicubicBezierPatch::join2(BicubicBezierPatch* other, GLuint thisSide, GLuint otherSide)
{
    BicubicBezierPatch* join = new BicubicBezierPatch();
    GLuint Xindex[4][4];
    GLuint Yindex[4][4];
    rotX((thisSide+2)%4 - otherSide, Xindex);
    rotY((thisSide+2)%4 - otherSide, Yindex);

    if (thisSide%2 == 1)
    {
        for (int i = 0; i < 4; i++)
        {
            join->SetData((thisSide == 1)*3,i,_data((thisSide == 3)*3,i));
            join->SetData((thisSide == 1) + 1,i, 2*_data((thisSide == 3)*3,i) - _data((thisSide == 3) + 1,i));
            join->SetData((thisSide == 3)*3,i,other->_data(Xindex[(thisSide == 1)*3][i], Yindex[(thisSide == 1)*3][i]));
            join->SetData((thisSide == 3) + 1,i, 2*join->_data((thisSide == 3)*3,i) - other->_data(Xindex[(thisSide == 1) + 1][i], Yindex[(thisSide == 1) + 1][i]));
        }
    }
    else
    {
        for (int i = 0; i < 4; i++)
        {
            join->SetData(i,(thisSide == 0)*3,_data(i,(thisSide == 2)*3));
            join->SetData(i,(thisSide == 0) + 1, 2*_data(i,(thisSide == 2)*3) - _data(i,(thisSide == 2) + 1));
            join->SetData(i,(thisSide == 2)*3,other->_data(Xindex[i][(thisSide == 0)*3], Yindex[i][(thisSide == 0)*3]));
            join->SetData(i,(thisSide == 2) + 1, 2*join->_data(i,(thisSide == 4)*3) - other->_data(Xindex[i][(thisSide == 0) + 1], Yindex[i][(thisSide == 0) + 1]));
        }
    }

    _attached[thisSide] = join;
    _attached_side[thisSide] = (thisSide + 2)%4;

    join->_attached[(thisSide + 2)%4] = this;
    join->_attached_side[(thisSide + 2)%4] = thisSide;

    join->_attached[thisSide] = other;
    join->_attached_side[thisSide] = otherSide;

    other->_attached[otherSide] = join;
    other->_attached_side[otherSide] = thisSide;

    return join;
}

void BicubicBezierPatch::getConn(std::vector<BicubicBezierPatch*>& ind, GLint ret[8])
{
    for (GLuint i = 0; i < 8; i++)
    {
        auto index = std::find(ind.begin(), ind.end(), _attached[i]);
        if (index == ind.end())
            ret[i] = -1;
        else
            ret[i] = index - ind.begin();
    }
}

void BicubicBezierPatch::setConn(std::vector<BicubicBezierPatch*>& ind, GLint indexes[8])
{
    for (GLuint i = 0; i < 8; i++)
    {
        if (indexes[i] == -1)
            _attached[i] = nullptr;
        else
            _attached[i] = ind[indexes[i]];
    }
}


void BicubicBezierPatch::set_selected_shader(int value)
{
    if (value == _selected_shader)
        return;
    _selected_shader = value;
}

void BicubicBezierPatch::set_refl_scale(double value)
{
    if (value == _reflScaleFactor)
        return;
    _reflScaleFactor = value;
}

void BicubicBezierPatch::set_refl_smoothing(double value)
{
    if (value == _reflSmoothing)
        return;
    _reflSmoothing = value;
}

void BicubicBezierPatch::set_refl_shading(double value)
{
    if (value == _reflShading)
        return;
    _reflShading = value;
}

void BicubicBezierPatch::set_toon_color_R(double value)
{
    if (value == _toonOutlineColor[0])
        return;
    _toonOutlineColor[0] = value / 255;
}

void BicubicBezierPatch::set_toon_color_G(double value)
{
    if (value == _toonOutlineColor[1])
        return;
    _toonOutlineColor[1] = value / 255;
}

void BicubicBezierPatch::set_toon_color_B(double value)
{
    if (value == _toonOutlineColor[2])
        return;
    _toonOutlineColor[2] = value / 255;
}

void BicubicBezierPatch::set_toon_color_A(double value)
{
    if (value == _toonOutlineColor[3])
        return;
    _toonOutlineColor[3] = value / 255;
}

int BicubicBezierPatch::get_selected_shader()
{
    return _selected_shader;
}

double BicubicBezierPatch::get_refl_scale()
{
    return  _reflScaleFactor;
}

double BicubicBezierPatch::get_refl_smoothing()
{
    return _reflSmoothing;
}

double BicubicBezierPatch::get_refl_shading()
{
    return  _reflShading;
}

double BicubicBezierPatch::get_toon_color_R()
{
    return _toonOutlineColor[0];
}

double BicubicBezierPatch::get_toon_color_G()
{
    return _toonOutlineColor[1];
}

double BicubicBezierPatch::get_toon_color_B()
{
    return _toonOutlineColor[2];
}

double BicubicBezierPatch::get_toon_color_A()
{
    return _toonOutlineColor[3];
}
