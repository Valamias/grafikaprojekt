#pragma once

#include <cmath>
#include <GL/glew.h>
#include <iostream>

namespace cagd
{
    //--------------------------------------
    // 3-dimensional homogeneous coordinates
    //--------------------------------------
    class HCoordinate3
    {
    protected:
        GLfloat _data[4]; // x, y, z, w;

    public:
        // default constructor
        HCoordinate3();

        // special constructor
        HCoordinate3(GLfloat x, GLfloat y, GLfloat z = 0.0, GLfloat w = 1.0);

        // homework: get components by value
        GLfloat operator[](GLuint rhs) const
		{
			return _data[rhs];
		}
        GLfloat x() const
		{
			return _data[0];
		}
        GLfloat y() const
		{
			return _data[1];
		}
        GLfloat z() const
		{
			return _data[2];
		}
        GLfloat w() const
		{
			return _data[3];
		}

        // homework: get components by reference
        GLfloat& operator[](GLuint rhs)
		{
			return _data[rhs];
		}
        GLfloat& x()
		{
			return _data[0];
		}
        GLfloat& y()
		{
			return _data[1];
		}
        GLfloat& z()
		{
			return _data[2];
		}
        GLfloat& w()
		{
			return _data[3];
		}

        // add
        const HCoordinate3 operator +(const HCoordinate3& rhs) const
		{
			return HCoordinate3(_data[0] * rhs._data[3] + _data[3] * rhs._data[0],
								_data[1] * rhs._data[3] + _data[3] * rhs._data[1],
								_data[2] * rhs._data[3] + _data[3] * rhs._data[2],
								_data[3] * rhs._data[3]);
		}

        // homework: add to this
        HCoordinate3& operator +=(const HCoordinate3& rhs)
		{
			_data[0] *= rhs._data[3];
			_data[0] += rhs._data[0] * _data[3];
			_data[1] *= rhs._data[3];
			_data[1] += rhs._data[1] * _data[3];
			_data[2] *= rhs._data[3];
			_data[2] += rhs._data[2] * _data[3];
			_data[3] *= rhs._data[3];
			return *this;
		}

        // homework: subtract
        const HCoordinate3 operator -(const HCoordinate3& rhs) const
		{
			return HCoordinate3(_data[0]*rhs._data[3]-_data[3]*rhs._data[0],
								_data[1]*rhs._data[3]-_data[3]*rhs._data[1],
								_data[2]*rhs._data[3]-_data[3]*rhs._data[2],
								_data[3]*rhs._data[3]);
		}

        // homework: subtract from this
        HCoordinate3& operator -=(const HCoordinate3& rhs)
		{
			_data[0] *= rhs._data[3];
			_data[0] -= rhs._data[0] * _data[3];
			_data[1] *= rhs._data[3];
			_data[1] -= rhs._data[1] * _data[3];
			_data[2] *= rhs._data[3];
			_data[2] -= rhs._data[2] * _data[3];
			_data[3] *= rhs._data[3];
			return *this;
		}

        // homework: dot product
        GLfloat operator *(const HCoordinate3& rhs) const
		{
			return (_data[0] * rhs._data[0] + _data[1] * rhs._data[1] + _data[2] * rhs._data[2]) / (_data[3] * rhs._data[3]);
		}

        // homework: cross product
        const HCoordinate3 operator ^(const HCoordinate3& rhs) const
		{
			return HCoordinate3(_data[1] * rhs._data[2] - _data[2] * rhs._data[1],
								_data[2] * rhs._data[0] - _data[0] * rhs._data[2],
								_data[0] * rhs._data[1] - _data[1] * rhs._data[0],
								_data[3] * rhs._data[3]);
		}

        // homework: cross product with this
        HCoordinate3& operator ^=(const HCoordinate3& rhs)
		{
            GLfloat newx, newy;

			newx = _data[1] * rhs._data[2] - _data[2] * rhs._data[1];
			newy = _data[2] * rhs._data[0] - _data[0] * rhs._data[2];
			_data[2] = _data[0] * rhs._data[1] - _data[1] * rhs._data[0];

			_data[0] -= newx;
			_data[1] -= newy;
			_data[3] *= rhs._data[3];
	
			return *this;	
		}

        // homework: multiplicate with scalar from right
        const HCoordinate3 operator *(GLfloat rhs) const
		{
			return HCoordinate3(_data[0],_data[1],_data[2],_data[3] / rhs);
		}

        // homework: multiplicate this with a scalar
        HCoordinate3& operator *=(GLfloat rhs)
		{
			_data[3] /= rhs;
			return *this;
		}

        // homework: divide with scalar
        const HCoordinate3 operator /(GLfloat rhs) const
		{
			return HCoordinate3(_data[0],_data[1],_data[2],_data[3] * rhs);
		}

        // homework: divide this with a scalar
        HCoordinate3& operator /=(GLfloat rhs)
		{
			_data[3] *= rhs;
			return *this;
		}

        // homework: length of vector represented by this homogeneous coordinate
        GLfloat length() const
		{
			return std::sqrt((*this) * (*this));
		}

        // homework: normalize
        HCoordinate3& normalize()
		{
            GLfloat l = length();

			if (l && l != 1.0)
				*this /= l;

			return *this;
		}
    };

    // default constructor
    inline HCoordinate3::HCoordinate3()
    {
        _data[0] = _data[1] = _data[2] = 0.0;
        _data[3] = 1.0;
    }

    // special constructor
    inline HCoordinate3::HCoordinate3(GLfloat x, GLfloat y, GLfloat z, GLfloat w)
    {
        _data[0] = x;
        _data[1] = y;
        _data[2] = z;
        _data[3] = w;
    }

    // homework: scale from left with a scalar
    inline const HCoordinate3 operator *(GLfloat lhs, const HCoordinate3& rhs)
	{
		return HCoordinate3(rhs.x(),rhs.y(),rhs.z(),rhs.w() * lhs);
	}

    // homework: output to stream
    inline std::ostream& operator <<(std::ostream& lhs, const HCoordinate3& rhs)
    {
        return lhs << rhs.x() << ' ' << rhs.y() << ' ' << rhs.z() << ' ' << rhs.w() << std::endl;
    }

    // homework: input from stream
    inline std::istream& operator >>(std::istream& lhs, HCoordinate3& rhs)
    {
        return lhs >> rhs.x() >> rhs.y() >> rhs.z() >> rhs.w();
    }
}
