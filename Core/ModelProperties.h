#pragma once

#include <vector>
#include <GL/glew.h>
#include "DCoordinates3.h"
#include "Materials.h"

namespace cagd
{
    struct ModelProperties
    {
        GLuint       id = 0;        // identifies a model, i.e., _model[id]
        DCoordinate3 position;  // new position of the middle point of the model's bounding box
        GLdouble     angle[3] = {0,0,0};  // rotational angles
        GLdouble     scale[3] = {1,1,1};  // scaling parameters
        GLfloat      color[3] = {1,1,1};
        Material*    mat = nullptr;

        friend std::ostream& operator << (std::ostream&, const ModelProperties& rhs);
        friend std::istream& operator >> (std::istream&, ModelProperties& rhs);
    };

    inline std::ostream& operator << (std::ostream& out, const ModelProperties& rhs)
    {
        out << rhs.id << std::endl;
        out << rhs.position << std::endl;
        out << rhs.angle[0] << ' ' << rhs.angle[1] << ' ' << rhs.angle[2] << std::endl;
        out << rhs.scale[0] << ' ' << rhs.scale[1] << ' ' << rhs.scale[2] << std::endl;
        out << rhs.color[0] << ' ' << rhs.color[1] << ' ' << rhs.color[2] << std::endl;
        if (rhs.mat == &MatFBBrass)
        {
            return out << 1 << std::endl;
        }
        if (rhs.mat == &MatFBGold)
        {
            return out << 2 << std::endl;
        }
        if (rhs.mat == &MatFBSilver)
        {
            return out << 3 << std::endl;
        }
        if (rhs.mat == &MatFBEmerald)
        {
            return out << 4 << std::endl;
        }
        if (rhs.mat == &MatFBPearl)
        {
            return out << 5 << std::endl;
        }
        if (rhs.mat == &MatFBRuby)
        {
            return out << 6 << std::endl;
        }
        if (rhs.mat == &MatFBTurquoise)
        {
            return out << 7 << std::endl;
        }
        return out << 0 << std::endl;
    }

    inline std::istream& operator >> (std::istream& in, ModelProperties& rhs)
    {
        in >> rhs.id;
        in >> rhs.position;
        in >> rhs.angle[0] >> rhs.angle[1] >> rhs.angle[2];
        in >> rhs.scale[0] >> rhs.scale[1] >> rhs.scale[2];
        in >> rhs.color[0] >> rhs.color[1] >> rhs.color[2];
        int mat;
        in >> mat;
        switch(mat)
        {
            case 1:
                rhs.mat = &MatFBBrass;
            break;
            case 2:
                rhs.mat = &MatFBGold;
            break;
            case 3:
                rhs.mat = &MatFBSilver;
            break;
            case 4:
                rhs.mat = &MatFBEmerald;
            break;
            case 5:
                rhs.mat = &MatFBPearl;
            break;
            case 6:
                rhs.mat = &MatFBRuby;
            break;
            case 7:
                rhs.mat = &MatFBTurquoise;
            break;
            default:
                rhs.mat = nullptr;

        }
        return in;
    }
}
